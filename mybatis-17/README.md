#### Mybatis缓存

​	MyBatis 中的缓存就是说 MyBatis 在执行一次SQL查询或者SQL更新之后，这条SQL语句并不会消失，而是被MyBatis 缓存起来，当再次执行相同SQL语句的时候，就会直接从缓存中进行提取，而不是再次执行SQL命令。

​	MyBatis中的缓存分为一级缓存和二级缓存，一级缓存又被称为 SqlSession 级别的缓存，二级缓存又被称为表级缓存。



#### 一级缓存

Mybatis 默认情况下自动开启，不允许关闭

![image-20230104161401212](img/image-20230104161401212.png)

​	每个SqlSession中持有了Executor，每个Executor中有一个LocalCache。当用户发起查询时，MyBatis根据当前执行的语句生成MappedStatement，在Local Cache进行查询，如果缓存命中的话，直接返回结果给用户，如果缓存没有命中的话，查询数据库，结果写入Local Cache，最后返回结果给用户。



#### 分析

1. 缓存是在 SqlSession 中的 Executor 中，所以它只存在于 SqlSession 层面，不同的 SqlSession 是不共用一级缓存的
2. 代码中使用 `CacheKey` 类来封装了缓存的 key，只有 key 相同，才能共享该数据，key的生成规则：[mappedStatementId + offset + limit + SQL + queryParams + environment] 



#### 总结

​	只有在 **参数** 和 **SQL** 完全一样的情况下，并且使用 **同一个 SqlSession**  的情况下，Mybatis 才会将第一次的查询结果缓存起来，后续同一个SqlSession的再查询，就会命中缓存，而不是去直接查库

