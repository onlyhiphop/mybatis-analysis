package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Test;

import java.io.IOException;

/**
 * @author bcliao
 * @date 2023/1/3 16:41
 */
public class MainTest {
    
    @Test
    public void test_cacheKey() throws IOException {

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        IUserDao dao = sqlSession.getMapper(IUserDao.class);

        User u = new User();
        u.setId(1);
        dao.queryUserById(u);
        
//        sqlSession.clearCache();
        
        dao.queryUserById(u);
        
    }
    
}
