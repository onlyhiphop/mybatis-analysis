package org.bcliao.ibatis.cache;

/**
 * @author bcliao
 * @date 2022/12/20 14:13
 */
public class NullCacheKey extends CacheKey{

    private static final long  serialVersionUID = 3704229911977019465L;

    public NullCacheKey() {
        super();
    }
    
}
