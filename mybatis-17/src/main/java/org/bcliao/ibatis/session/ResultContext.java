package org.bcliao.ibatis.session;

/**
 * 
 * 结果上下文
 * 
 * @Author bcliao
 * @Date 2022/9/26 11:01
 */
public interface ResultContext {

    /**
     * 获取结果
     */
    Object getResultObject();

    /**
     * 获取记录数
     */
    int getResultCount();

}