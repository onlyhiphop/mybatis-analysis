package org.bcliao.ibatis.session.defaults;

import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.mapping.Environment;
import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.TransactionIsolationLevel;
import org.bcliao.ibatis.transaction.Transaction;
import org.bcliao.ibatis.transaction.TransactionFactory;

import java.sql.SQLException;

/**
 * @author: bcliao
 * @date: 2022/8/5 15:07
 **/
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSqlSession() {
        Transaction tx = null;
        try {
            final Environment environment = configuration.getEnvironment();
            TransactionFactory transactionFactory = environment.getTransactionFactory();
            tx = transactionFactory.newTransaction(environment.getDataSource(), TransactionIsolationLevel.READ_COMMITTED, false);
            //创建执行器
            final Executor executor = configuration.newExecutor(tx);
            //创建DefaultSqlSession
            return new DefaultSqlSession(configuration, executor);
        }catch (Exception e){
            try {
                assert tx != null;
                tx.close();
            }catch (SQLException ignored){}
            throw new RuntimeException("Error opening session.  Cause: " + e);
        }
    }
}
