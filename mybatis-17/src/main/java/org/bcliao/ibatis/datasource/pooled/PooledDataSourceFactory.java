package org.bcliao.ibatis.datasource.pooled;

import org.bcliao.ibatis.datasource.unpooled.UnpooledDataSourceFactory;


/**
 *
 * 有连接池的数据源工厂
 *
 * @author: bcliao
 * @date: 2022/8/15 10:34
 **/
public class PooledDataSourceFactory extends UnpooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
