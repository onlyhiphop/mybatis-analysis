package org.bcliao.ibatis.builder.xml;

import org.bcliao.ibatis.builder.BaseBuilder;
import org.bcliao.ibatis.builder.MapperBuilderAssistant;
import org.bcliao.ibatis.builder.ResultMapResolver;
import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.mapping.ResultFlag;
import org.bcliao.ibatis.mapping.ResultMap;
import org.bcliao.ibatis.mapping.ResultMapping;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * XML 映射构建起
 * 
 * @Author bcliao
 * @Date 2022/8/31 8:53
 */
public class XMLMapperBuilder extends BaseBuilder {
    
    private Element element;
    private String resource;
    //映射器构建助手
    private MapperBuilderAssistant builderAssistant;
    
    public XMLMapperBuilder(InputStream inputStream, Configuration configuration, String resource) throws DocumentException {
        this(new SAXReader().read(inputStream), configuration, resource);
    }
    
    public XMLMapperBuilder(Document document, Configuration configuration, String resource){
        super(configuration);
        this.builderAssistant = new MapperBuilderAssistant(configuration, resource);
        this.element = document.getRootElement();
        this.resource = resource;
    }

    /**
     * 解析
     */
    public void parse() throws Exception {
        // 如果当前资源没有加载过再加载，防止重复加载
        if(!configuration.isResourceLoaded(resource)){
            configurationElement(element);
            // 标记一下，已经加载过了
            configuration.addLoadedResource(resource);
            // 绑定映射器到namespace Mybatis 源码方法名 -> bindMapperForNamespace
            configuration.addMapper(Resources.classForName(builderAssistant.getCurrentNamespace()));
        }
    }

    // 配置mapper元素
    // <mapper namespace="org.mybatis.example.BlogMapper">
    //   <select id="selectBlog" parameterType="int" resultType="Blog">
    //    select * from Blog where id = #{id}
    //   </select>
    // </mapper>
    private void configurationElement(Element element) {
        // 1. 配置 namespace
        String namespace = element.attributeValue("namespace");
        if("".endsWith(namespace)){
            throw new RuntimeException("Mapper's namespace cannot be empty");
        }
        builderAssistant.setCurrentNamespace(namespace);
        
        // 解析 ResultMap
        resultMapElements(element.elements("resultMap"));
        
        // 2. 配置 select|insert|update|delete
        buildStatementFromContext(element.elements("select"), 
                element.elements("insert"),
                element.elements("update"),
                element.elements("delete"));
    }

    private void resultMapElements(List<Element> list) {
        for (Element element : list) {
            try {
                resultMapElement(element, Collections.emptyList());    
            }catch (Exception ex){
            }
        }
    }

    /**
     * <resultMap id="activityMap" type="cn.bugstack.mybatis.test.po.Activity">
     * <id column="id" property="id"/>
     * <result column="activity_id" property="activityId"/>
     * <result column="activity_name" property="activityName"/>
     * <result column="activity_desc" property="activityDesc"/>
     * <result column="create_time" property="createTime"/>
     * <result column="update_time" property="updateTime"/>
     * </resultMap>
     */
    private ResultMap resultMapElement(Element element, List<ResultMapping> additionalResultMappings) {
        String id = element.attributeValue("id");
        String type = element.attributeValue("type");
        Class<?> typeClass = resolveClass(type);
        
        List<ResultMapping> resultMappings = new ArrayList<>();
        resultMappings.addAll(additionalResultMappings);

        List<Element> resultChildren = element.elements();
        for (Element resultChild : resultChildren) {
            List<ResultFlag> flags = new ArrayList<>();
            if("id".equals(resultChild.getName())){
                flags.add(ResultFlag.ID);
            }
            // 构建 ResultMapping
            resultMappings.add(buildResultMappingFromContext(resultChild, typeClass, flags));
        }
        
        // 创建结果映射解析器
        ResultMapResolver resultMapResolver = new ResultMapResolver(builderAssistant, id, typeClass, resultMappings);
        return resultMapResolver.resolve();
    }

    /**
     * <id column="id" property="id"/>
     * <result column="activity_id" property="activityId"/>
     */
    private ResultMapping buildResultMappingFromContext(Element context, Class<?> resultType, List<ResultFlag> flags) {
        String property = context.attributeValue("property");
        String column = context.attributeValue("column");
        return builderAssistant.buildResultMapping(resultType, property, column, flags);
    }

    // 配置 select | insert | update | delete 
    private void buildStatementFromContext(List<Element>... lists) {
        for (List<Element> list : lists) {
            for (Element element : list) {
                final XMLStatementBuilder statementBuilder = new XMLStatementBuilder(configuration, builderAssistant, element);
                statementBuilder.parseStatementNode();
            }
        }
    }

}