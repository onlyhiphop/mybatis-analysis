package org.bcliao.ibatis.executor.result;

import org.bcliao.ibatis.reflection.factory.ObjectFactory;
import org.bcliao.ibatis.session.ResultContext;
import org.bcliao.ibatis.session.ResultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 默认结果处理器
 * 
 * @Author bcliao
 * @Date 2022/9/26 10:58
 */
public class DefaultResultHandler implements ResultHandler {

    private final List<Object> list;
    
    public DefaultResultHandler(){
        this.list = new ArrayList<>();
    }

    /**
     * 通过 ObjectFactory 反射工具类，产生特定的 List
     */
    @SuppressWarnings("unchecked")
    public DefaultResultHandler(ObjectFactory objectFactory) {
        this.list = objectFactory.create(List.class);
    }


    @Override
    public void handleResult(ResultContext context) {
        list.add(context.getResultObject());
    }

    public List<Object> getResultList() {
        return list;
    }
}