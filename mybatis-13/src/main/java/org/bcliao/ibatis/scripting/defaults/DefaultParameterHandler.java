package org.bcliao.ibatis.scripting.defaults;

import com.alibaba.fastjson.JSON;
import org.bcliao.ibatis.executor.parameter.ParameterHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.mapping.ParameterMapping;
import org.bcliao.ibatis.reflection.MetaObject;
import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.type.JdbcType;
import org.bcliao.ibatis.type.TypeHandler;
import org.bcliao.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * 
 * 默认参数处理器
 * 
 * @Author bcliao
 * @Date 2022/9/20 14:09
 */
public class DefaultParameterHandler implements ParameterHandler {
    
    private Logger logger = LoggerFactory.getLogger(DefaultParameterHandler.class);
    
    private final TypeHandlerRegistry typeHandlerRegistry;
    
    private final MappedStatement mappedStatement;
    private final Object parameterObject;
    private BoundSql boundSql;
    private Configuration configuration;
    
    public DefaultParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql){
        this.mappedStatement = mappedStatement;
        this.configuration = mappedStatement.getConfiguration();
        this.typeHandlerRegistry = mappedStatement.getConfiguration().getTypeHandlerRegistry();
        this.parameterObject = parameterObject;
        this.boundSql = boundSql;
    }
    
    @Override
    public Object getParameterObject() {
        return parameterObject;
    }

    @Override
    public void setParameters(PreparedStatement ps) throws SQLException {
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        if(null != parameterMappings){
            //遍历参数
            for (int i = 0; i < parameterMappings.size(); i++) {
                ParameterMapping parameterMapping = parameterMappings.get(i);
                String propertyName = parameterMapping.getProperty();
                Object value;
                if(typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())){
                    value = parameterObject;
                }else{
                    //通过 MetaObject.getValue 反射取得值设进去
                    MetaObject metaObject = configuration.newMetaObject(parameterObject);
                    value = metaObject.getValue(propertyName);
                }
                JdbcType jdbcType = parameterMapping.getJdbcType();
                
                //设置参数
                logger.info("根据每个 ParameterMapping 中的 TypeHandler 设置对应的参数信息 value：{} ", JSON.toJSONString(value));

                //不同策略的TypeHandler是放在parameterMapping中的，所以这里需要注意，什么时候把typeHandler构造到parameterMapping中
                TypeHandler typeHandler = parameterMapping.getTypeHandler();
                typeHandler.setParameter(ps, i + 1, value, jdbcType);
            }
        }
    }
}