package org.bcliao.ibatis.builder;

import org.bcliao.ibatis.mapping.ResultMap;
import org.bcliao.ibatis.mapping.ResultMapping;

import java.util.List;

/**
 * @author bcliao
 * @date 2022/11/28 15:01
 */
public class ResultMapResolver {
    
    private final MapperBuilderAssistant assistant;
    private String id;
    private Class<?> type;
    private List<ResultMapping> resultMappings;

    public ResultMapResolver(MapperBuilderAssistant assistant, String id, Class<?> type, List<ResultMapping> resultMappings) {
        this.assistant = assistant;
        this.id = id;
        this.type = type;
        this.resultMappings = resultMappings;
    }
    
    public ResultMap resolve(){
        return assistant.addResultMap(this.id, this.type, this.resultMappings);
    }
}
