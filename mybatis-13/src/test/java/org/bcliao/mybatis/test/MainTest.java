package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author bcliao
 * @date 2022/11/29 9:41
 */
public class MainTest {

    private SqlSession sqlSession;
    private IUserDao userDao;
    
    @Before
    public void init() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        sqlSession = sqlSessionFactory.openSqlSession();
        userDao = sqlSession.getMapper(IUserDao.class);
    }
    
    @Test
    public void testSelect(){
        User user = userDao.queryUserInfoById(1);
        System.out.println(user);
    }
}
