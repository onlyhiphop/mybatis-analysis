package org.bcliao.mybatis.test.dao;

import org.bcliao.mybatis.test.po.User;

/**
 * @Author bcliao
 * @Date 2022/8/29 16:49
 */
public interface IUserDao {

    User queryUserInfoById(Integer id);
    
}