package org.bcliao.ibatis.builder.xml;

import org.bcliao.ibatis.builder.BaseBuilder;
import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * 
 * XML 映射构建起
 * 
 * @Author bcliao
 * @Date 2022/8/31 8:53
 */
public class XMLMapperBuilder extends BaseBuilder {
    
    private Element element;
    private String resource;
    
    // 一个 namespace 对应一个 dao 接口，也对应一个xml文件
    private String currentNamespace;
    
    public XMLMapperBuilder(InputStream inputStream, Configuration configuration, String resource) throws DocumentException {
        this(new SAXReader().read(inputStream), configuration, resource);
    }
    
    public XMLMapperBuilder(Document document, Configuration configuration, String resource){
        super(configuration);
        this.element = document.getRootElement();
        this.resource = resource;
    }

    /**
     * 解析
     */
    public void parse() throws Exception {
        // 如果当前资源没有加载过再加载，防止重复加载
        if(!configuration.isResourceLoaded(resource)){
            configurationElement(element);
            // 标记一下，已经加载过了
            configuration.addLoadedResource(resource);
            // 绑定映射器到 namespace
            configuration.addMapper(Resources.classForName(currentNamespace));
        }
    }

    // 配置mapper元素
    // <mapper namespace="org.mybatis.example.BlogMapper">
    //   <select id="selectBlog" parameterType="int" resultType="Blog">
    //    select * from Blog where id = #{id}
    //   </select>
    // </mapper>
    private void configurationElement(Element element) {
        // 1. 配置 namespace
        currentNamespace = element.attributeValue("namespace");
        if("".endsWith(currentNamespace)){
            throw new RuntimeException("Mapper's namespace cannot be empty");
        }
        // 2. 配置 select|insert|update|delete
        buildStatementFromContext(element.elements("select"));
    }

    // 配置 select | insert | update | delete 
    private void buildStatementFromContext(List<Element> list) {
        for (Element element : list) {
            final XMLStatementBuilder statementBuilder = new XMLStatementBuilder(configuration, element, currentNamespace);
            statementBuilder.parseStatementNode();
        }
    }

}