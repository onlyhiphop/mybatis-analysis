package org.bcliao.ibatis.type;

import java.sql.PreparedStatement;

/**
 * 
 * 类型处理器
 * 
 * @Author bcliao
 * @Date 2022/8/31 9:49
 */
public interface TypeHandler<T> {

    /**
     * 设置参数
     */
    void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType);

}