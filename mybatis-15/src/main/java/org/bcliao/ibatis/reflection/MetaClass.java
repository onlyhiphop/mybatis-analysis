package org.bcliao.ibatis.reflection;

import org.bcliao.ibatis.reflection.invoker.GetFieldInvoker;
import org.bcliao.ibatis.reflection.invoker.Invoker;
import org.bcliao.ibatis.reflection.invoker.MethodInvoker;
import org.bcliao.ibatis.reflection.property.PropertyTokenizer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 *
 * 元类
 *
 * Reflector 反射器类提供的是最基础的核心功能，很多方法也都是私有的，为了更加方便的使用，还需要做一层元类的包装。
 * 在元类 MetaClass 提供必要的创建反射器以及使用反射器获取 get/set 的 Invoker 反射方法。
 *
 * @author: bcliao
 * @date: 2022/8/18 09:44
 **/
public class MetaClass {

    //真正反射调用的是最底层还是 reflector
    private Reflector reflector;

    private MetaClass(Class<?> type){
        this.reflector = Reflector.forClass(type);
    }

    public static MetaClass forClass(Class<?> type){
        return new MetaClass(type);
    }

    public static boolean isClassCacheEnabled(){
        return Reflector.isClassCacheEnabled();
    }

    public static void setClassCacheEnabled(boolean classCacheEnabled){
        Reflector.setClassCacheEnabled(classCacheEnabled);
    }

    public MetaClass metaClassForProperty(String name){
        Class<?> getterType = reflector.getGetterType(name);
        return MetaClass.forClass(getterType);
    }

    // 得到真正的属性名称链 如：传入 Order[0].User.name 最后得到 order.user.name
    public String findProperty(String name){
        StringBuilder prop = buildProperty(name, new StringBuilder());
        return prop.length() > 0 ? prop.toString() : null;
    }

    public String findProperty(String name, boolean useCameCaseMapping){
        if(useCameCaseMapping){
            name = name.replace("_", "");
        }
        return findProperty(name);
    }

    public String[] getGetterNames(){
        return reflector.getGetablePropertyNames();
    }

    public String[] getSetterNames(){
        return reflector.getSetablePropertyNames();
    }

    // 获取最后一个属性的 set 参数Class类型 如： order.user.name  name的类型为 String
    public Class<?> getSetterType(String name){
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if(prop.hasNext()){
            MetaClass metaProp = metaClassForProperty(prop.getName());
            return metaProp.getSetterType(prop.getChildren());
        }else{
            return reflector.getSetterType(prop.getName());
        }
    }

    public Class<?> getGetterType(String name){
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if(prop.hasNext()){
            MetaClass metaProp = metaClassForProperty(prop);
            return metaProp.getGetterType(prop.getChildren());
        }
        return getGetterType(prop);
    }

    public MetaClass metaClassForProperty(PropertyTokenizer prop){
        Class<?> propType = getGetterType(prop);
        return MetaClass.forClass(propType);
    }

    public Class<?> getGetterType(PropertyTokenizer prop){
        Class<?> getterType = reflector.getGetterType(prop.getName());
        if(prop.getIndex() != null && Collection.class.isAssignableFrom(getterType)){
            Type returnType = getGenericGetterType(prop.getName());
            if(returnType instanceof ParameterizedType){
                Type[] actualTypeArguments = ((ParameterizedType)returnType).getActualTypeArguments();
                if(actualTypeArguments != null && actualTypeArguments.length == 1){
                    returnType = actualTypeArguments[0];
                    if(returnType instanceof Class){
                        getterType = (Class<?>) returnType;
                    } else if (returnType instanceof ParameterizedType) {
                        getterType = (Class<?>) ((ParameterizedType) returnType).getRawType();
                    }
                }
            }
        }
        return getterType;
    }

    private Type getGenericGetterType(String propertyName) {
        try {
            Invoker invoker = reflector.getGetInvoker(propertyName);
            if (invoker instanceof MethodInvoker) {
                Field _method = MethodInvoker.class.getDeclaredField("method");
                _method.setAccessible(true);
                Method method = (Method) _method.get(invoker);
                return method.getGenericReturnType();
            } else if (invoker instanceof GetFieldInvoker) {
                Field _field = GetFieldInvoker.class.getDeclaredField("field");
                _field.setAccessible(true);
                Field field = (Field) _field.get(invoker);
                return field.getGenericType();
            }
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
        return null;
    }

    public boolean hasSetter(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            if (reflector.hasSetter(prop.getName())) {
                MetaClass metaProp = metaClassForProperty(prop.getName());
                return metaProp.hasSetter(prop.getChildren());
            } else {
                return false;
            }
        } else {
            return reflector.hasSetter(prop.getName());
        }
    }

    public boolean hasGetter(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            if (reflector.hasGetter(prop.getName())) {
                MetaClass metaProp = metaClassForProperty(prop);
                return metaProp.hasGetter(prop.getChildren());
            } else {
                return false;
            }
        } else {
            return reflector.hasGetter(prop.getName());
        }
    }

    public Invoker getGetInvoker(String name) {
        return reflector.getGetInvoker(name);
    }

    public Invoker getSetInvoker(String name) {
        return reflector.getSetInvoker(name);
    }


    // 得到 真正 的属性名称链 如： order.user.name
    private StringBuilder buildProperty(String name, StringBuilder builder) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if(prop.hasNext()){
            String propertyName = reflector.findPropertyName(prop.getName());
            if(propertyName != null){
                builder.append(propertyName);
                builder.append(".");
                MetaClass metaProp = metaClassForProperty(propertyName);
                metaProp.buildProperty(prop.getChildren(), builder);
            }
        }else{
            String propertyName = reflector.findPropertyName(name);
            if(propertyName != null){
                builder.append(propertyName);
            }
        }
        return builder;
    }

    public boolean hasDefaultConstructor() {
        return reflector.hasDefaultConstructor();
    }
}
