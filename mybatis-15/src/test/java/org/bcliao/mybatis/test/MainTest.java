package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author bcliao
 * @date 2022/12/7 13:36
 */
public class MainTest {
    
    private IUserDao userDao;
    
    @Before
    public void init() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();
        userDao = sqlSession.getMapper(IUserDao.class);
    }
    
    @Test
    public void test_query(){
        User user = new User();
        user.setId(1);
        User u = userDao.queryUserById(user);
        System.out.println(u);
    }
    
}
