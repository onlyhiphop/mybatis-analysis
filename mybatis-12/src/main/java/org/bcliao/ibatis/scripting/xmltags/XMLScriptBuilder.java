package org.bcliao.ibatis.scripting.xmltags;

import org.bcliao.ibatis.builder.BaseBuilder;
import org.bcliao.ibatis.mapping.SqlSource;
import org.bcliao.ibatis.scripting.defaults.RawSqlSource;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * XML 脚本构建器
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:38
 */
public class XMLScriptBuilder extends BaseBuilder {
    
    private Element element;
    private boolean isDynamic;
    private Class<?> parameterType;
    
    public XMLScriptBuilder(Configuration configuration, Element element, Class<?> parameterType) {
        super(configuration);
        this.element = element;
        this.parameterType = parameterType;
    }
    
    public SqlSource parseScriptNode(){
        List<SqlNode> contents = parseDynamicTags(element);
        MixedSqlNode rootSqlNode = new MixedSqlNode(contents);
        return new RawSqlSource(configuration, rootSqlNode, parameterType);
    }
    
    public List<SqlNode> parseDynamicTags(Element element){
        List<SqlNode> contents = new ArrayList<>();
        // element.getText 拿到 Sql
        String data = element.getText();
        contents.add(new StaticTextSqlNode(data));
        return contents;
    }
}