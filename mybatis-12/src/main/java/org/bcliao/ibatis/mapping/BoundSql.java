package org.bcliao.ibatis.mapping;

import org.bcliao.ibatis.reflection.MetaObject;
import org.bcliao.ibatis.session.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 绑定的 SQl ，是从 SqlSource 而来, 将动态内容都处理完成得到 SQL 语句字符串，其中包括 ? ，还有绑定的参数
 *
 * @author: bcliao
 * @date: 2022/8/10 14:18
 **/
public class BoundSql {

    private String sql;
    private List<ParameterMapping> parameterMappings;
    private Object parameterObject;
    private Map<String, Object> additionalParameters;
    private MetaObject metaParameters;


    public BoundSql(Configuration configuration, String sql, List<ParameterMapping> parameterMappings, Object parameterObject) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
        this.parameterObject = parameterObject;
        this.additionalParameters= new HashMap<>();
        this.metaParameters = configuration.newMetaObject(additionalParameters);
    }

    public String getSql() {
        return sql;
    }

    public Object getParameterObject() {
        return parameterObject;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }
    
    public boolean hasAdditionalParameter(String name){
        return  metaParameters.hasGetter(name);
    }
    
    public void setAdditionalParameters(String name, Object value){
        metaParameters.setValue(name, value);
    }
    
    public Object getAdditionalParameter(String name){
        return metaParameters.getValue(name);
    }
}