package org.bcliao.ibatis.transaction.jdbc;

import org.bcliao.ibatis.session.TransactionIsolationLevel;
import org.bcliao.ibatis.transaction.Transaction;
import org.bcliao.ibatis.transaction.TransactionFactory;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * JdbcTransaction 工厂
 *
 * @author: bcliao
 * @date: 2022/8/10 11:14
 **/
public class JdbcTransactionFactory implements TransactionFactory {
    @Override
    public Transaction newTransaction(Connection conn) {
        return new JdbcTransaction(conn);
    }

    @Override
    public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit) {
        return new JdbcTransaction(dataSource, level, autoCommit);
    }
}
