package org.bcliao.ibatis.session;

/**
 * 
 * 分页记录限制
 * 
 * @Author bcliao
 * @Date 2022/9/26 13:35
 */
public class RowBounds {
    
    public static final int NO_ROW_OFFSET = 0;
    public static final int NO_ROW_LIMIT = Integer.MAX_VALUE;
    public static final RowBounds DEFAULT = new RowBounds();
    
    // offset , limit 就等于一般分页的 start , limit
    private int offset;
    private int limit;
    
    // 默认是一页 Integer.MAX_VALUE 条
    public RowBounds(){
        this.offset = NO_ROW_OFFSET;
        this.limit = NO_ROW_LIMIT;
    }

    public RowBounds(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

}