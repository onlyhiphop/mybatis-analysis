package org.bcliao.ibatis.session;

import org.bcliao.ibatis.binding.MapperRegistry;
import org.bcliao.ibatis.datasource.druid.DruidDataSourceFactory;
import org.bcliao.ibatis.datasource.pooled.PooledDataSourceFactory;
import org.bcliao.ibatis.datasource.unpooled.UnpooledDataSourceFactory;
import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.executor.SimpleExecutor;
import org.bcliao.ibatis.executor.parameter.ParameterHandler;
import org.bcliao.ibatis.executor.resultset.DefaultResultSetHandler;
import org.bcliao.ibatis.executor.resultset.ResultSetHandler;
import org.bcliao.ibatis.executor.statement.PreparedStatementHandler;
import org.bcliao.ibatis.executor.statement.StatementHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.Environment;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.mapping.ResultMap;
import org.bcliao.ibatis.reflection.MetaObject;
import org.bcliao.ibatis.reflection.factory.DefaultObjectFactory;
import org.bcliao.ibatis.reflection.factory.ObjectFactory;
import org.bcliao.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.bcliao.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.bcliao.ibatis.scripting.LanguageDriver;
import org.bcliao.ibatis.scripting.LanguageDriverRegistry;
import org.bcliao.ibatis.scripting.xmltags.XMLLanguageDriver;
import org.bcliao.ibatis.transaction.Transaction;
import org.bcliao.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.bcliao.ibatis.type.TypeAliasRegistry;
import org.bcliao.ibatis.type.TypeHandlerRegistry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * 这里是所有 mapper 接口 (xml文件) 对应一个 configuration
 *
 * @author: bcliao
 * @date: 2022/8/8 09:39
 **/
public class Configuration {

    /** 环境 */
    protected Environment environment;
    /** 映射注册机 */
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);
    /** 类型别名注册机 */
    protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry();

    /**
     * 映射的语句， 存在 Map 里
     * key: 接口方法的全路径名称（含包名）
     */
    protected final Map<String, MappedStatement> mappedStatements = new HashMap<>();
    
    /** 结果映射，存在 Map 里 */
    protected final Map<String, ResultMap> resultMaps = new HashMap<>();
    
    /** 类型处理器注册机 */
    protected final TypeHandlerRegistry typeHandlerRegistry = new TypeHandlerRegistry();
    protected final LanguageDriverRegistry languageRegistry = new LanguageDriverRegistry();
    
    /** 对象工厂和对象包装器工厂 */
    protected ObjectFactory objectFactory = new DefaultObjectFactory();
    protected ObjectWrapperFactory objectWrapperFactory = new DefaultObjectWrapperFactory();
    
    // 存下已经加载过的资源，防止重复加载
    protected final Set<String> loadedResources = new HashSet<>();
    
    protected String databaseId;

    public Configuration(){
        typeAliasRegistry.registerAlias("JDBC", JdbcTransactionFactory.class);
        typeAliasRegistry.registerAlias("DRUID", DruidDataSourceFactory.class);
        typeAliasRegistry.registerAlias("UNPOOLED", UnpooledDataSourceFactory.class);
        typeAliasRegistry.registerAlias("POOLED", PooledDataSourceFactory.class);
        
        languageRegistry.setDefaultDriverClass(XMLLanguageDriver.class);
    }

    public void addMappers(String packageName){
        mapperRegistry.addMappers(packageName);
    }

    public <T> void addMapper(Class<T> type){
        mapperRegistry.addMapper(type);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession){
        return mapperRegistry.getMapper(type, sqlSession);
    }

    public boolean hasMapper(Class<?> type){
        return mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms){
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(String id){
        return mappedStatements.get(id);
    }

    public TypeAliasRegistry getTypeAliasRegistry() {
        return typeAliasRegistry;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    /**
     * 创建结果集处理器
     * @return
     */
    public ResultSetHandler newResultSetHandler(Executor executor, MappedStatement mappedStatement, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql){
        return new DefaultResultSetHandler(executor, mappedStatement, resultHandler, rowBounds, boundSql);
    }

    /**
     * 产生执行器
     */
    public Executor newExecutor(Transaction transaction){
        return new SimpleExecutor(this, transaction);
    }

    /**
     * 创建语句处理器
     */
    public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        return new PreparedStatementHandler(executor, mappedStatement, parameter, rowBounds, resultHandler, boundSql);
    }

    /**
     * 创建元对象
     */
    public MetaObject newMetaObject(Object object){
        return MetaObject.forObject(object,objectFactory, objectWrapperFactory);
    }

    /**
     * 获取类型处理器注册机
     */
    public TypeHandlerRegistry getTypeHandlerRegistry() {
        return typeHandlerRegistry;
    }

    /**
     * 创建参数处理器
     */
    public ParameterHandler newParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql){
        // 参数处理器
        ParameterHandler parameterHandler = mappedStatement.getLang().createParameterHandler(mappedStatement, parameterObject, boundSql);
        // 插件的一些参数，也是在这里处理，暂时不添加这部分内容 interceptorChain.pluginAll(parameterHandler);
        return parameterHandler;
    }
    
    public LanguageDriver getDefaultScriptingLanguageInstance(){
        return languageRegistry.getDefaultDriver();
    }
    
    public boolean isResourceLoaded(String resource){
        return loadedResources.contains(resource);
    }
    
    public void addLoadedResource(String resource){
        loadedResources.add(resource);
    }

    public LanguageDriverRegistry getLanguageRegistry() {
        return languageRegistry;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public ObjectFactory getObjectFactory() {
        return objectFactory;
    }

    public ResultMap getResultMap(String id) {
        return resultMaps.get(id);
    }

    public void addResultMap(ResultMap resultMap) {
        resultMaps.put(resultMap.getId(), resultMap);
    }
    
}