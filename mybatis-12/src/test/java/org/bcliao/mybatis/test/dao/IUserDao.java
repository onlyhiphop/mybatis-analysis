package org.bcliao.mybatis.test.dao;

import org.bcliao.ibatis.annotations.Delete;
import org.bcliao.ibatis.annotations.Insert;
import org.bcliao.ibatis.annotations.Select;
import org.bcliao.ibatis.annotations.Update;
import org.bcliao.mybatis.test.po.User;

import java.util.List;

/**
 * @author bcliao
 * @date 2022/11/24 9:42
 */
public interface IUserDao {
    
    @Select("SELECT id, name, age, create_time as createTime, update_time as updateTime\n" +
            "FROM t_user\n" +
            "where id = #{id}")
    User queryUserInfoById(Long id);

    @Select("SELECT id, name, age, create_time as createTime, update_time as updateTime\n" +
            "FROM t_user\n" +
            "WHERE id = #{id}")
    User queryUserInfo(User req);

    @Select("SELECT id, name, age, create_time as createTime, update_time as updateTime\n" +
            "FROM t_user limit 10\n")
    List<User> queryUserInfoList();
    
    @Update("UPDATE t_user\n" +
            "SET name=#{name}\n" +
            "WHERE id = #{id}")
    int updateUserInfo(User req);
    
    @Insert("INSERT INTO t_user\n" +
            "(name, age, create_time, update_time)\n" +
            "VALUES (#{name}, #{age}, now(), now())")
    void insertUserInfo(User req);
    
    @Delete("DELETE FROM t_user WHERE id = #{id}")
    int deleteUserInfoById(Long id);
}
