package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author bcliao
 * @date 2022/11/24 10:00
 */
public class MainTest {
    
    private SqlSession sqlSession;
    private IUserDao userDao;
    
    @Before
    public void init() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        sqlSession = sqlSessionFactory.openSqlSession();
        
        userDao = sqlSession.getMapper(IUserDao.class);
    }
    
    
    @Test
    public void testSelect(){
        User user = userDao.queryUserInfoById(1L);
        System.out.println(user);

        System.out.println("=================>");
        
        User u1 = new User();
        u1.setId(2);
        User user2 = userDao.queryUserInfo(u1);
        System.out.println(user2);

        System.out.println("=================>");
        
        List<User> userList = userDao.queryUserInfoList();
        System.out.println(userList);
    }
    
    @Test
    public void testUpdate(){
        User req = new User();
        req.setId(1);
        req.setName("1046修改");
        int count = userDao.updateUserInfo(req);
        System.out.println(count);
    }
    
    @Test
    public void testInsert(){
        User req = new User();
        req.setAge(99);
        req.setName("1049添加");
        userDao.insertUserInfo(req);
    }
    
    @Test
    public void testDelete(){
        userDao.deleteUserInfoById(99L);
    }
}
