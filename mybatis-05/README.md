### 设计

![图 6-1 池化数据源设计](img/mybatis-220423-01.png)

- 通过提供统一的连接池中心，存放数据源链接，并根据配置按照请求获取链接的操作，创建连接池的数据源链接数量。这里就包括了最大空闲链接和最大活跃链接，都随着创建过程被控制
- 此外由于控制了连接池中连接的数量，所以当外部从连接池获取链接时，如果链接已满则会进行循环等待。这也是大家日常使用DB连接池，如果一个SQL操作引起了慢查询，则会导致整个服务进入瘫痪的阶段，各个和数据库相关的接口调用，都不能获得到链接，接口查询TP99陡然增高，系统开始大量报警。那连接池可以配置的很大吗，也不可以，因为连接池要和数据库所分配的连接池对应上，避免应用配置连接池超过数据库所提供的连接池数量，否则会出现夯住不能分配链接的问题，导致数据库拖垮从而引起连锁反应。



### 核心类结构

![图 6-2 池化数据源核心类关系](img/mybatis-220423-02.png)

- 在 Mybatis 数据源的实现中，包括两部分分为无池化的 UnpooledDataSource 实现类和有池化的 PooledDataSource 实现类，池化的实现类 PooledDataSource 以对无池化的 UnpooledDataSource 进行扩展处理。把创建出来的链接保存到内存中，记录为空闲链接和活跃链接，在不同的阶段进行使用。
- PooledConnection 是对链接的代理操作，通过invoke方法的反射调用，对关闭的链接进行回收处理，并使用 notifyAll 通知正在等待链接的用户进行抢链接。
- 另外是对 DataSourceFactory 数据源工厂接口的实现，由无池化工厂实现后，有池化工厂继承的方式进行处理，这里没有太多的复杂操作，池化的处理主要集中在 PooledDataSource 类中进行处理。