package org.bcliao.mybatis.test;

import com.alibaba.fastjson.JSON;
import org.bcliao.ibatis.datasource.pooled.PooledDataSource;
import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Test;

import java.sql.Connection;

/**
 * @author: bcliao
 * @date: 2022/8/15 13:50
 **/
public class MainTest {

    @Test
    public void test_SqlSessionFactory() throws Exception {
        // 1. 从 SqlSessionFactory 获取 SqlSession
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        // 2. 获取映射器对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        for (int i = 0; i < 50; i++) {
            User user = userDao.queryUserInfoById(i);
            System.out.println((JSON.toJSONString(user)));
        }
    }


    @Test
    public void test_pooled() throws Exception{
        PooledDataSource pooledDataSource = new PooledDataSource();
        pooledDataSource.setDriver("com.mysql.jdbc.Driver");
        pooledDataSource.setUrl("jdbc:mysql://47.110.49.220:3306/index_test?useUnicode=true");
        pooledDataSource.setUsername("root");
        pooledDataSource.setPassword("123456");
        // 持续获得链接
        while (true){
            Connection connection = pooledDataSource.getConnection();
            System.out.println(connection);
            Thread.sleep(1000);
            connection.close();
        }
    }

}
