package org.bcliao.ibatis.builder;

import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.type.TypeAliasRegistry;

/**
 *
 * 构建器的基类，建造者模式
 *
 * @author: bcliao
 * @date: 2022/8/8 09:35
 **/
public abstract class BaseBuilder {

    protected final Configuration configuration;
    protected final TypeAliasRegistry typeAliasRegistry;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        this.typeAliasRegistry = this.configuration.getTypeAliasRegistry();
    }

    public Configuration getConfiguration() {
        return configuration;
    }

}
