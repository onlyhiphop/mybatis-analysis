package org.bcliao.ibatis.session;

/**
 *
 * SqlSession 是用来执行 SQl 获取映射器 管理事务
 * 通常情况下，我们在应用程序中使用的 mybatis 的 API 就是这个接口定义的方法
 * 一个 mapper interface 对应一个 SqlSession
 *
 * @author: bcliao
 * @date: 2022/8/5 14:33
 **/
public interface SqlSession {


    /**
     * 返回指定的 SqlID 获取一条记录的封装对象
     * @param statement SqlId 对应 mybatis 就是 mapper 接口的方法名（xml文件中的id）
     * @param <T> 封装之后的对象类型
     * @return 封装之后的对象
     */
    <T> T selectOne(String statement);

    /**
     * 根据指定的 SqlId 获取一条记录的封装对象，只不过这个方法容许我们可以给sql传递一些参数
     * 一般在实际使用中，这个参数传递的是pojo，或者Map或者ImmutableMap(不可变的map)
     * @param statement sqlId
     * @param parameter 传递的参数
     * @param <T> 封装后对象类型
     * @return 封装后对象
     */
    <T> T selectOne(String statement, Object parameter);

    /**
     * 得到配置
     */
    Configuration getConfiguration();

    /**
     * 得到映射器，这个巧妙的使用了泛型 使得类型安全
     * 到了 Mybatis3 还可以使用注解 ，这样xml都不用写了
     * @param type
     * @param <T>
     * @return
     */
    <T> T getMapper(Class<T> type);
}
