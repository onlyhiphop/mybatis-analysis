package org.bcliao.ibatis.session;

import org.bcliao.ibatis.binding.MapperRegistry;
import org.bcliao.ibatis.datasource.druid.DruidDataSourceFactory;
import org.bcliao.ibatis.datasource.pooled.PooledDataSourceFactory;
import org.bcliao.ibatis.datasource.unpooled.UnpooledDataSourceFactory;
import org.bcliao.ibatis.mapping.Environment;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.bcliao.ibatis.type.TypeAliasRegistry;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 这里是所有 mapper 接口 (xml文件) 对应一个 configuration
 *
 * @author: bcliao
 * @date: 2022/8/8 09:39
 **/
public class Configuration {

    /** 环境 */
    protected Environment environment;
    /** 映射注册机 */
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);
    /** 类型别名注册机 */
    protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry();

    /**
     * 映射的语句， 存在 Map 里
     * key: 接口方法的全路径名称（含包名）
     */
    protected final Map<String, MappedStatement> mappedStatements = new HashMap<>();

    public Configuration(){
        typeAliasRegistry.registerAlias("JDBC", JdbcTransactionFactory.class);
        typeAliasRegistry.registerAlias("DRUID", DruidDataSourceFactory.class);
        typeAliasRegistry.registerAlias("UNPOOLED", UnpooledDataSourceFactory.class);
        typeAliasRegistry.registerAlias("POOLED", PooledDataSourceFactory.class);
    }

    public void addMappers(String packageName){
        mapperRegistry.addMappers(packageName);
    }

    public <T> void addMapper(Class<T> type){
        mapperRegistry.addMapper(type);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession){
        return mapperRegistry.getMapper(type, sqlSession);
    }

    public boolean hasMapper(Class<?> type){
        return mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms){
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(String id){
        return mappedStatements.get(id);
    }

    public TypeAliasRegistry getTypeAliasRegistry() {
        return typeAliasRegistry;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
