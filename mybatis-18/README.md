

> 引用文章： https://zhuanlan.zhihu.com/p/454070563



#### 二级缓存

![image-20230104161401212](img/image-20230104161401212.png)

优先级先查二级缓存，再查一级缓存，最后查数据库



​	MyBatis的二级缓存是Application级别的缓存，它可以提高对数据库查询的效率，以提高应用的性能。

​	范围是按照每个namepace缓存来存贮和维护，同一个namespace放到一个缓存对象中，当这个namaspace中执行了！insert、update和delete语句的时候，整个namespace中的缓存全部清除掉。



#### 避免使用二级缓存

​	二级缓存不像一级缓存那样查询完直接放入一级缓存，而是要等事务提交时才会将查询出来的数据放到二级缓存中，如果事务1查出来直接放到二级缓存，此时事务2从二级缓存中拿到了事务1缓存的数据，但是事务1回滚了，此时事务2不就发生了脏读了吗？

​	涉及到多表的查询时，很容易出现脏读问题，比如其中两个表肯定是在不同的 namespace中，如果其中一个 update 了 ，那该多表查询必须要清楚缓存，用 cache-ref 引用某个 namespace 下的缓存，可以让两个 namespace 使用同一个缓存，但是这样做后续代码增多，系统复杂，很容易漏，出现问题



#### 总结

​	mybatis的一级缓存和二级缓存都是基于本地的，分布式环境下必然会出现脏读（在一个机子查，另一个机子改，这样缓存不能清除）。

​	二级缓存可以通过实现Cache接口，来集中管理缓存，避免脏读，但是有一定的开发成本，并且在多表查询时，使用不当极有可能会出现脏数据。

**「除非对性能要求特别高，否则一级缓存和二级缓存都不建议使用」**