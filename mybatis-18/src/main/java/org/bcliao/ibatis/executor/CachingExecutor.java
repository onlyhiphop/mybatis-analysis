package org.bcliao.ibatis.executor;

import com.alibaba.fastjson.JSON;
import org.bcliao.ibatis.cache.Cache;
import org.bcliao.ibatis.cache.CacheKey;
import org.bcliao.ibatis.cache.TransactionalCacheManager;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.session.ResultHandler;
import org.bcliao.ibatis.session.RowBounds;
import org.bcliao.ibatis.transaction.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * 
 * 二级缓存执行器
 * 
 * @author bcliao
 * @date 2023/1/6 17:35
 */
public class CachingExecutor implements Executor{

    private Logger logger = LoggerFactory.getLogger(CachingExecutor.class);
    
    private Executor delegate;
    private TransactionalCacheManager tcm = new TransactionalCacheManager();

    public CachingExecutor(Executor delegate) {
        this.delegate = delegate;
        delegate.setExecutorWrapper(this);
    }

    @Override
    public int update(MappedStatement ms, Object parameter) throws SQLException {
        return delegate.update(ms, parameter);
    }

    @Override
    public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
        //默认情况下是没有开启缓存的(二级缓存).要开启二级缓存,你需要在你的 SQL 映射文件中添加一行: <cache/>
        Cache cache = ms.getCache();
        if(cache != null){
            flushCacheIfRequired(ms);
            if(ms.isUseCache() && resultHandler == null){
                // 先查CacheKey ，查不到再委托给实际的执行器去查
                List<E> list = (List<E>) tcm.getObject(cache, key);
                if(list == null){
                    list = delegate.<E>query(ms, parameter, rowBounds, resultHandler, key, boundSql);
                    // cache：缓存队列实现类，FIFO
                    // key：哈希值 [mappedStatementId + offset + limit + SQL + queryParams + environment]
                    // list: 查询的数据
                    tcm.putObject(cache, key, list);
                }
                // 打印调试日志，记录二级缓存获取数据
                if(logger.isDebugEnabled() && cache.getSize() > 0){
                    logger.debug("二级缓存：{}", JSON.toJSONString(list));
                }
                return list;
            }
        }
        return delegate.<E>query(ms, parameter, rowBounds, resultHandler, key, boundSql);
    }

    @Override
    public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException {
        // 1. 获取绑定 SQL
        BoundSql boundSql = ms.getBoundSql(parameter);
        // 2. 创建缓存 key
        CacheKey key = createCacheKey(ms, parameter, rowBounds, boundSql);
        return query(ms, parameter, rowBounds, resultHandler, key, boundSql);
    }

    @Override
    public Transaction getTransaction() {
        return delegate.getTransaction();
    }

    @Override
    public void commit(boolean required) throws SQLException {
        delegate.commit(required);
        tcm.commit();
    }

    @Override
    public void rollback(boolean required) throws SQLException {
        try {
            delegate.rollback(required);
        } finally {
            if (required){
                tcm.rollback();
            }
        }
    }

    @Override
    public void close(boolean forceRollBack) {
        try {
            if(forceRollBack){
                tcm.rollback();
            } else {
                tcm.commit();
            }
        } finally {
            delegate.close(forceRollBack);
        }
    }

    @Override
    public void clearLocalCache() {
        delegate.clearLocalCache();
    }

    @Override
    public CacheKey createCacheKey(MappedStatement ms, Object parameterObject, RowBounds rowBounds, BoundSql boundSql) {
        return delegate.createCacheKey(ms, parameterObject, rowBounds, boundSql);
    }

    @Override
    public void setExecutorWrapper(Executor executor) {
        throw new UnsupportedOperationException("This method should not be called");
    }


    private void flushCacheIfRequired(MappedStatement ms) {
        Cache cache = ms.getCache();
        if(cache != null && ms.isFlushCacheRequired()){
            tcm.clear(cache);
        }
    }
}
