package org.bcliao.ibatis.plugin;

/**
 * 
 * 方法签名 用于定位需要在哪个类的哪个方法下完成插件的调用
 * 
 * @author bcliao
 * @date 2022/12/12 10:01
 */
public @interface Signature {

    /**
     * 被拦截类
     */
    Class<?> type();

    /**
     * 被拦截类的方法
     */
    String method();

    /**
     * 被拦截类的方法的参数
     */
    Class<?>[] args();

}
