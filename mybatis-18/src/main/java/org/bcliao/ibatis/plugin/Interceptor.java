package org.bcliao.ibatis.plugin;

import java.util.Properties;

/**
 * 
 * 拦截器接口
 * 
 * @author bcliao
 * @date 2022/12/8 15:24
 */
public interface Interceptor {
    
    // 拦截，使用方实现
    Object intercept(Invocation invocation) throws Throwable;
    
    // 代理
    default Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }
    
    // 设置属性
    default void setProperties(Properties properties){
        
    }
}
