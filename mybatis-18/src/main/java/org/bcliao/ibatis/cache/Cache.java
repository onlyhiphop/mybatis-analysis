package org.bcliao.ibatis.cache;

/**
 * 
 * SPI （Service Provider Interface） for cache providers 缓存接口
 * 
 * @author bcliao
 * @date 2022/12/20 14:12
 */
public interface Cache {

    /**
     * 获取 ID，每个缓存都有唯一 ID 标识
     */
    String getId();

    /**
     * 存入值
     */
    void putObject(Object key, Object value);

    /**
     * 获取值
     */
    Object getObject(Object key);

    /**
     * 删除值
     */
    Object removeObject(Object key);

    /**
     * 清空
     */
    void clear();

    /**
     * 获取缓存大小
     */
    int getSize();
}
