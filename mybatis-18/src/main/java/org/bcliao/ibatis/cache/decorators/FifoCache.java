package org.bcliao.ibatis.cache.decorators;

import org.bcliao.ibatis.cache.Cache;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 
 * FIFO (first in, first out) cache decorator
 * 
 * FIFO缓存
 * 这个类就是维护一个 FIFO 链表，其他都委托给所包装的 cache 去做，典型的装饰模式
 * 
 * @author bcliao
 * @date 2023/1/5 11:06
 */
public class FifoCache implements Cache {
    
    private final Cache delegate;
    private Deque<Object> keyList;
    private int size;

    public FifoCache(Cache delegate) {
        this.delegate = delegate;
        this.keyList = new LinkedList<>();
        this.size = 1024;
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public void putObject(Object key, Object value) {
        cycleKeyList(key);
        delegate.putObject(key, value);
    }

    private void cycleKeyList(Object key) {
        keyList.addLast(key);
        if(keyList.size() > size){
            Object oldestKey = keyList.removeFirst();
            delegate.removeObject(oldestKey);
        }
    }

    @Override
    public Object getObject(Object key) {
        return delegate.getObject(key);
    }

    @Override
    public Object removeObject(Object key) {
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        delegate.clear();
        keyList.clear();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }

    public void setSize(int size) {
        this.size = size;
    }
}
