package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Test;

import java.io.IOException;

/**
 * @author bcliao
 * @date 2023/1/9 10:42
 */
public class MainTest {
    
    @Test
    public void test_cache() throws IOException {

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));

        SqlSession sqlSession = sqlSessionFactory.openSqlSession();
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        User u = new User();
        u.setId(1);
        User result = userDao.queryUserById(u);
        System.out.println(result);

        // 只有事务提交了，才会放入二级缓存中
        sqlSession.commit();
        
        SqlSession sqlSession2 = sqlSessionFactory.openSqlSession();
        IUserDao userDao2 = sqlSession2.getMapper(IUserDao.class);
        userDao2.queryUserById(u);
        
    }
    
}
