package org.bcliao.ibatis.reflection.wrapper;

import org.bcliao.ibatis.reflection.MetaObject;

/**
 *
 * 默认对象包装工厂
 *
 * @author: bcliao
 * @date: 2022/8/19 09:05
 **/
public class DefaultObjectWrapperFactory implements ObjectWrapperFactory {
    @Override
    public boolean hasWrapperFor(Object object) {
        return false;
    }

    @Override
    public ObjectWrapper getWrapperFor(MetaObject metaObject, Object object) {
        throw new RuntimeException("The DefaultObjectWrapperFactory should never be called to provide an ObjectWrapper.");
    }

}
