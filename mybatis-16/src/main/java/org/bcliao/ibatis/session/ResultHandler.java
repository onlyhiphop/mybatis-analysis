package org.bcliao.ibatis.session;

/**
 * 
 * 结果处理器
 * 
 * @author: bcliao
 * @date: 2022/8/16 09:39
 **/
public interface ResultHandler {

    /**
     * 处理结果
     */
    void handleResult(ResultContext context);
    
}