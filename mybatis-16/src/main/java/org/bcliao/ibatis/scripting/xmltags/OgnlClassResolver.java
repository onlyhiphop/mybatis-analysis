package org.bcliao.ibatis.scripting.xmltags;

import ognl.ClassResolver;
import org.bcliao.ibatis.io.Resources;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author bcliao
 * @date 2022/12/6 16:17
 */
public class OgnlClassResolver implements ClassResolver {
    
    private Map<String, Class<?>> classes = new HashMap<>(101);
    
    @Override
    public Class classForName(String className, Map map) throws ClassNotFoundException {
        Class<?> result = null;
        if((result = classes.get(className)) == null){
            try {
                result = Resources.classForName(className);
            } catch (ClassNotFoundException e1){
                if(className.indexOf('.') == -1){
                    result = Resources.classForName("java.lang." + className);
                    classes.put("java.lang." + className, result);
                }
            }
            classes.put(className, result);
        }
        return result;
    }
}
