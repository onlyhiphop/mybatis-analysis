package org.bcliao.ibatis.scripting.xmltags;

/**
 * 
 * IF SQL 节点
 * 
 * @author bcliao
 * @date 2022/12/6 15:17
 */
public class IfSqlNode implements SqlNode{
    
    private ExpressionEvaluator evaluator;
    private String test;
    private SqlNode contents;
    
    public IfSqlNode(SqlNode contents, String test){
        this.test = test;
        this.contents = contents;
        this.evaluator = new ExpressionEvaluator();
    }
    
    @Override
    public boolean apply(DynamicContext context) {
        // 如果满足条件，则 apply，并返回 true
        if(evaluator.evaluateBoolean(test, context.getBindings())){
            contents.apply(context);
            return true;
        }
        return false;
    }
}
