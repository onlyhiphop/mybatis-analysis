package org.bcliao.mybatis.test.plugin;

import org.bcliao.ibatis.executor.parameter.ParameterHandler;
import org.bcliao.ibatis.executor.resultset.ResultSetHandler;
import org.bcliao.ibatis.executor.statement.StatementHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.plugin.Interceptor;
import org.bcliao.ibatis.plugin.Intercepts;
import org.bcliao.ibatis.plugin.Invocation;
import org.bcliao.ibatis.plugin.Signature;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Properties;

/**
 * @author bcliao
 * @date 2022/12/8 15:22
 */
@Intercepts({
        @Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class}),
        @Signature(type = ParameterHandler.class, method = "setParameters", args = {PreparedStatement.class})
})
public class TestPlugin implements Interceptor {
    
    @Override
    public Object intercept(Invocation invocation) throws Throwable {

        String name = invocation.getMethod().getName();
        if ("prepare".equalsIgnoreCase(name)) {
            // 获取 StatementHandler
            StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
            // 获取 SQl 信息
            BoundSql boundSql = statementHandler.getBoundSql();
            String sql = boundSql.getSql();
            // 输出 SQL
            System.out.println("拦截SQL：" + sql);
        }
        else if("setParameters".equalsIgnoreCase(name)){
            ParameterHandler parameterHandler = (ParameterHandler) invocation.getTarget();
            System.out.println("参数：" + parameterHandler.getParameterObject().getClass().getName());
        }

        System.out.println("args[]: " + Arrays.asList(invocation.getArgs()));
       
        // 放行
        return invocation.proceed();
    }

    @Override
    public void setProperties(Properties properties) {
        System.out.println("参数输出：" + properties.getProperty("test00"));
    }
}
