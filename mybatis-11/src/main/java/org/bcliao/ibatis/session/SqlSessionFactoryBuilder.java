package org.bcliao.ibatis.session;

import org.bcliao.ibatis.builder.xml.XMLConfigBuilder;
import org.bcliao.ibatis.session.defaults.DefaultSqlSessionFactory;

import java.io.Reader;

/**
 * 作为整个 mybatis 的入口
 * 构建 SqlSessionFactory 的工厂
 *
 * @author: bcliao
 * @date: 2022/8/8 09:26
 **/
public class SqlSessionFactoryBuilder {

    public SqlSessionFactory build(Reader reader){
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(reader);
        return build(xmlConfigBuilder.parse());
    }

    public SqlSessionFactory build(Configuration configuration){
        return new DefaultSqlSessionFactory(configuration);
    }
}
