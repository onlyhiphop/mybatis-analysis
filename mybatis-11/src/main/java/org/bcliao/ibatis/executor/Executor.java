package org.bcliao.ibatis.executor;

import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.session.ResultHandler;
import org.bcliao.ibatis.session.RowBounds;
import org.bcliao.ibatis.transaction.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * 执行器
 *
 * @author: bcliao
 * @date: 2022/8/15 18:46
 **/
public interface Executor {

    ResultHandler NO_RESULT_HANDLER = null;

    int update(MappedStatement ms, Object parameter) throws SQLException;
    
    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException;

    Transaction getTransaction();

    void commit(boolean required) throws SQLException;

    void rollback(boolean required) throws SQLException;

    void close(boolean forceRollBack);
}
