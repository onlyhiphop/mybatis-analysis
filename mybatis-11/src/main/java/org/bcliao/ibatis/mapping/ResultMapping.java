package org.bcliao.ibatis.mapping;

import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.type.JdbcType;
import org.bcliao.ibatis.type.TypeHandler;

/**
 * 
 * 结果映射
 * 
 * @Author bcliao
 * @Date 2022/9/26 10:32
 */
public class ResultMapping {
    
    private Configuration configuration;
    private String property;
    private String column;
    private Class<?> javaType;
    private JdbcType jdbcType;
    private TypeHandler<?> typeHandler;

    ResultMapping() {
    }

    public static class Builder {
        private ResultMapping resultMapping = new ResultMapping();


    }
    
}