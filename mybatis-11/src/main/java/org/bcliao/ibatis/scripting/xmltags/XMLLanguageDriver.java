package org.bcliao.ibatis.scripting.xmltags;

import org.bcliao.ibatis.executor.parameter.ParameterHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.mapping.SqlSource;
import org.bcliao.ibatis.scripting.LanguageDriver;
import org.bcliao.ibatis.scripting.defaults.DefaultParameterHandler;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Element;

/**
 * 
 * XML 语言驱动器
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:28
 */
public class XMLLanguageDriver implements LanguageDriver {
    
    @Override
    public SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType) {
        // 用 XML 脚本构建器解析
        XMLScriptBuilder builder = new XMLScriptBuilder(configuration, script, parameterType);
        return builder.parseScriptNode();
    }

    @Override
    public ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
        return new DefaultParameterHandler(mappedStatement, parameterObject, boundSql);
    }

}