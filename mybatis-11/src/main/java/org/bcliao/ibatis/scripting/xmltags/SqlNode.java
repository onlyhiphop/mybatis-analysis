package org.bcliao.ibatis.scripting.xmltags;

/**
 * 
 * SQL 节点
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:26
 */
public interface SqlNode {
    
    boolean apply(DynamicContext context);
    
}