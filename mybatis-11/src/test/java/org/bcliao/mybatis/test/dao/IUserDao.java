package org.bcliao.mybatis.test.dao;

import org.bcliao.mybatis.test.po.User;

import java.util.List;

/**
 * @Author bcliao
 * @Date 2022/8/29 16:49
 */
public interface IUserDao {

    User queryUserInfoById(Integer id);
    
    User queryUserInfo(User user);

    List<User> queryUserInfoList();

    int updateUserInfo(User req);

    void insertUserInfo(User req);

    int deleteUserInfoByUserId(Integer id);
}