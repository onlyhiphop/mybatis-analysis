package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * @author bcliao
 * @date 2022/10/27 11:35
 */
public class MainTest {
    
    private SqlSession sqlSession;
    private IUserDao userDao;
    
    @Before
    public void init() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        sqlSession = sqlSessionFactory.openSqlSession();
        
        userDao = sqlSession.getMapper(IUserDao.class);
    }
 
    @Test
    public void testSelectList(){
        List<User> lists = userDao.queryUserInfoList();
        for (User user : lists) {
            System.out.println(user);
        }
    }
    
    @Test
    public void testInsert(){
        User user = new User("1347测试", 47);
        userDao.insertUserInfo(user);
    }
    
    @Test
    public void testUpdate(){
        User user = new User();
        user.setId(1);
        user.setName("修改测试");
        userDao.updateUserInfo(user);
    }
    
    @Test
    public void testDelete(){
        userDao.deleteUserInfoByUserId(1);
    }
    
}
