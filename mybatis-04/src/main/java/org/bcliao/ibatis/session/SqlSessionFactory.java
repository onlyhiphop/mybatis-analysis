package org.bcliao.ibatis.session;

/**
 * 工厂模式接口，构建 SqlSession 的工厂
 * @author: bcliao
 * @date: 2022/8/5 14:52
 **/
public interface SqlSessionFactory {

    /**
     * 打开一个 session
     * @return SqlSession
     */
    SqlSession openSqlSession();

}
