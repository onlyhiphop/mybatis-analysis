package org.bcliao.mybatis.test.dao;

import org.bcliao.mybatis.test.po.User;


/**
 * @author: bcliao
 * @date: 2022/8/10 14:56
 **/
public interface IUserDao {

    User queryUserInfoById(Integer id);

}
