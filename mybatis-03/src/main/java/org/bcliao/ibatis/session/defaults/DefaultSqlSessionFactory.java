package org.bcliao.ibatis.session.defaults;

import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;

/**
 * @author: bcliao
 * @date: 2022/8/5 15:07
 **/
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSqlSession() {
        return new DefaultSqlSession(configuration);
    }
}
