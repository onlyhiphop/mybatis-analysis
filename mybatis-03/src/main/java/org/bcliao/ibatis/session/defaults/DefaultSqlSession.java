package org.bcliao.ibatis.session.defaults;

import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.session.SqlSession;

import java.util.Arrays;

/**
 * 默认SqlSession实现类
 * @author: bcliao
 * @date: 2022/8/5 14:50
 **/
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <T> T selectOne(String statement) {
        return (T) ("你被代理了！" + statement);
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        MappedStatement mappedStatement = configuration.getMappedStatement(statement);
        System.out.println(("你被代理了！" + "\n方法：" + statement + " \n入参：" + Arrays.toString((Object[])parameter) + "\n待执行SQL：" + mappedStatement.getSql()));
        return null;
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return configuration.getMapper(type, this);
    }
}
