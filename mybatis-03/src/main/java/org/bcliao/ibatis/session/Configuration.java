package org.bcliao.ibatis.session;

import org.bcliao.ibatis.binding.MapperRegistry;
import org.bcliao.ibatis.mapping.MappedStatement;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 这里是所有 mapper 接口 (xml文件) 对应一个 configuration
 *
 * @author: bcliao
 * @date: 2022/8/8 09:39
 **/
public class Configuration {

    /**
     * 映射注册机
     */
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);

    /**
     * 映射的语句， 存在 Map 里
     * key: 接口方法的全路径名称（含包名）
     */
    protected final Map<String, MappedStatement> mappedStatements = new HashMap<>();

    public void addMappers(String packageName){
        mapperRegistry.addMappers(packageName);
    }

    public <T> void addMapper(Class<T> type){
        mapperRegistry.addMapper(type);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession){
        return mapperRegistry.getMapper(type, sqlSession);
    }

    public boolean hasMapper(Class<?> type){
        return mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms){
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(String id){
        return mappedStatements.get(id);
    }

}
