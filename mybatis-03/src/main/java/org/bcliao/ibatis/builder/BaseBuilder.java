package org.bcliao.ibatis.builder;

import org.bcliao.ibatis.session.Configuration;

/**
 *
 * 构建器的基类，建造者模式
 *
 * @author: bcliao
 * @date: 2022/8/8 09:35
 **/
public abstract class BaseBuilder {

    protected final Configuration configuration;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

}
