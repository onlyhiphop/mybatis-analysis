package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IProductDao;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.ProductInfo;
import org.bcliao.mybatis.test.po.User;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;

/**
 * @author: bcliao
 * @date: 2022/8/9 09:37
 **/
public class MainTest {

    @Test
    public void test_SqlSessionFactory() throws IOException {

        // 1. 从 SqlSessionFactory 中获取 SqlSession
        Reader reader = Resources.getResourceAsReader("mybatis-config-datasource.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        // 2. 获取映射器对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        IProductDao productDao = sqlSession.getMapper(IProductDao.class);

        // 3. 测试验证
        User user = userDao.queryUserInfoById(1L);
        ProductInfo productInfo = productDao.queryProductInfoById(2L);

    }

}
