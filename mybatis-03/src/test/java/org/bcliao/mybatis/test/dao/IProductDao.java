package org.bcliao.mybatis.test.dao;

import org.bcliao.mybatis.test.po.ProductInfo;

/**
 * @author: bcliao
 * @date: 2022/8/9 09:43
 **/
public interface IProductDao {

    ProductInfo queryProductInfoById(Long id);

}
