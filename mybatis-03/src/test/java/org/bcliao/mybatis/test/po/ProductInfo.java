package org.bcliao.mybatis.test.po;

import java.time.LocalDateTime;

/**
 * @author: bcliao
 * @date: 2022/8/9 10:11
 **/
public class ProductInfo {

    private Integer id;
    private String productName;
    private LocalDateTime createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
