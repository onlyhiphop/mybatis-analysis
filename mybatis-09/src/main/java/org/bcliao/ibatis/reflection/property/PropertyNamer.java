package org.bcliao.ibatis.reflection.property;

import java.util.Locale;

/**
 *
 * 属性命名器
 *
 * @author: bcliao
 * @date: 2022/8/17 14:37
 **/
public class PropertyNamer {

    private PropertyNamer(){
    }

    /**
     * 方法转换为属性
     */
    public static String methodToProperty(String methodName){
        if(methodName.startsWith("is")){
            methodName = methodName.substring(2);
        }else if(methodName.startsWith("get") || methodName.startsWith("set")){
            methodName = methodName.substring(3);
        }else{
            throw new RuntimeException("Error parsing property name '" + methodName + "'.  Didn't start with 'is', 'get' or 'set'.");
        }
        /**
         * 如果只有 1 个字母，转换为小写
         * 如果大于 1 个字符，第二个字母非大写的话 转换为小写
         */
        if(methodName.length() == 1 || (methodName.length() > 1 && !Character.isUpperCase(methodName.charAt(1)))){
            methodName = methodName.substring(0, 1).toLowerCase(Locale.ENGLISH) + methodName.substring(1);
        }
        return methodName;
    }

}
