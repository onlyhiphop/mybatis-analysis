package org.bcliao.ibatis.scripting.xmltags;

/**
 * 
 * 静态文本 SQL 节点
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:45
 */
public class StaticTextSqlNode implements SqlNode{
    
    private String text;

    public StaticTextSqlNode(String text) {
        this.text = text;
    }

    @Override
    public boolean apply(DynamicContext context) {
        // 将文本加入context
        context.appendSql(text);
        return true;
    }
}