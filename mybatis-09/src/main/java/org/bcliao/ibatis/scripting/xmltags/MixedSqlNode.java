package org.bcliao.ibatis.scripting.xmltags;

import java.util.List;

/**
 * 
 * 混合SQL节点
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:59
 */
public class MixedSqlNode implements SqlNode{
    
    // 组合模式，拥有一个 SqlNode 的 list
    private List<SqlNode> contents;

    public MixedSqlNode(List<SqlNode> contents) {
        this.contents = contents;
    }

    @Override
    public boolean apply(DynamicContext context) {
        // 依次调用 list 里每个元素的 apply
        contents.forEach(node -> node.apply(context));
        return true;
    }
}