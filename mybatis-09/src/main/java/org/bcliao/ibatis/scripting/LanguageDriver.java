package org.bcliao.ibatis.scripting;

import org.bcliao.ibatis.executor.parameter.ParameterHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.mapping.SqlSource;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Element;

/**
 * 
 * 脚本语言驱动
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:11
 */
public interface LanguageDriver {

    /**
     * 创建SQL源码(mapper xml方式)
     */
    SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType);

    /**
     * 创建参数处理器
     */
    ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql);
}