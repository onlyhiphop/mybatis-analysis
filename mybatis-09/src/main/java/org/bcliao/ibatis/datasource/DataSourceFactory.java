package org.bcliao.ibatis.datasource;

import javax.sql.DataSource;
import java.util.Properties;

/**
 *
 * 数据源工厂
 *
 * @author: bcliao
 * @date: 2022/8/10 13:29
 **/
public interface DataSourceFactory {

    /**
     * 设置属性 ， 被 XMLConfigBuilder 所调用
     * @param props
     */
    void setProperties(Properties props);

    /**
     * 生产数据源,直接得到javax.sql.DataSource
     * @return
     */
    DataSource getDataSource();

}
