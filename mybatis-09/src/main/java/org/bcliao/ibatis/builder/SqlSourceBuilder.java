package org.bcliao.ibatis.builder;

import org.bcliao.ibatis.mapping.ParameterMapping;
import org.bcliao.ibatis.mapping.SqlSource;
import org.bcliao.ibatis.parsing.GenericTokenParser;
import org.bcliao.ibatis.parsing.TokenHandler;
import org.bcliao.ibatis.reflection.MetaClass;
import org.bcliao.ibatis.reflection.MetaObject;
import org.bcliao.ibatis.session.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * SQL 源码构建器
 * 
 * @Author bcliao
 * @Date 2022/9/2 13:19
 */
public class SqlSourceBuilder extends BaseBuilder{
    
    private static Logger logger = LoggerFactory.getLogger(SqlSourceBuilder.class);
    
    private static final String parameterProperties = "javaType,jdbcType,node,numericScale,resultMap,typeHandler,jdbcTypeName";
    
    public SqlSourceBuilder(Configuration configuration) {
        super(configuration);
    }
    
    public SqlSource parse(String originalSql, Class<?> parameterType, Map<String, Object> additionalParameters){
        ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler(configuration, parameterType, additionalParameters);
        GenericTokenParser parser = new GenericTokenParser("#{", "}", handler);

        //就是在这一步解析原始sql的时候，填充了对应的parameterMapping以及typeHandler
        String sql = parser.parse(originalSql);
        // 返回静态 sql
        return new StaticSqlSource(configuration, sql, handler.getParameterMappings());
    }
    
    private static class ParameterMappingTokenHandler extends BaseBuilder implements TokenHandler{

        private List<ParameterMapping> parameterMappings = new ArrayList<>();
        private Class<?> parameterType;
        private MetaObject metaParameters;

        public ParameterMappingTokenHandler(Configuration configuration, Class<?> parameterType, Map<String, Object> additionalParameters) {
            super(configuration);
            this.parameterType = parameterType;
            this.metaParameters = configuration.newMetaObject(additionalParameters);
        }

        public List<ParameterMapping> getParameterMappings() {
            return parameterMappings;
        }

        @Override
        public String handleToken(String content) {
            parameterMappings.add(buildParameterMapping(content));
            return "?";
        }
        
        // 构建参数映射
        private ParameterMapping buildParameterMapping(String content){
            // 先解析参数映射，就是转化成一个 HashMap | #{favouriteSection,jdbcType=VARCHAR}
            Map<String, String> propertiesMap = new ParameterExpression(content);
            String property = propertiesMap.get("property");
            Class<?> propertyType;
            if(typeHandlerRegistry.hasTypeHandler(parameterType)){
                propertyType = parameterType;
            }else if(property != null){
                MetaClass metaClass = MetaClass.forClass(parameterType);
                if(metaClass.hasGetter(property)){
                    propertyType = metaClass.getGetterType(property);
                }else{
                    propertyType = Object.class;
                }
            }else{
                propertyType = Object.class;
            }
            
            logger.info("构建参数映射 property: {} propertyType: {}", property, propertyType);
            ParameterMapping.Builder builder = new ParameterMapping.Builder(configuration, property, propertyType);
            return builder.build();
        }
    }
}