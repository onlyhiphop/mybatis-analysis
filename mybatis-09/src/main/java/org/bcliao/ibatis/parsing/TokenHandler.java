package org.bcliao.ibatis.parsing;

/**
 * 
 * 记号处理器
 * 
 * @Author bcliao
 * @Date 2022/9/2 14:10
 */
public interface TokenHandler {
    
    String handleToken(String content);
    
}