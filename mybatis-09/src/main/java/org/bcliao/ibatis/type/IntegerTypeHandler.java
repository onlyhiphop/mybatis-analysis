package org.bcliao.ibatis.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @Author bcliao
 * @Date 2022/9/20 16:13
 */
public class IntegerTypeHandler extends BaseTypeHandler<Integer>{
    @Override
    protected void setNonNullParameter(PreparedStatement ps, int i, Integer parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter);
    }
}