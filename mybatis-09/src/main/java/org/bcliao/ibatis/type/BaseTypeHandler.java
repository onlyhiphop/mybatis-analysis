package org.bcliao.ibatis.type;

import org.bcliao.ibatis.session.Configuration;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 
 * 类型处理器的基类
 * 
 * @Author bcliao
 * @Date 2022/9/19 17:57
 */
public abstract class BaseTypeHandler<T> implements TypeHandler<T> {

    protected Configuration configuration;
    
    public void setConfiguration(Configuration configuration){
        this.configuration = configuration;
    }
    
    @Override
    public void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException {
        //定义抽象方法，由子类实现不同类型的属性设置
        setNonNullParameter(ps, i, parameter, jdbcType);
    }

    protected abstract void setNonNullParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException;
}