package org.bcliao.ibatis.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 
 * Long 类型处理器
 * 
 * @Author bcliao
 * @Date 2022/9/19 18:04
 */
public class LongTypeHandler extends BaseTypeHandler<Long>{
    
    @Override
    protected void setNonNullParameter(PreparedStatement ps, int i, Long parameter, JdbcType jdbcType) throws SQLException {
        ps.setLong(i, parameter);    
    }
    
}