package org.bcliao.ibatis.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 
 * String 类型处理器
 * 
 * @Author bcliao
 * @Date 2022/9/19 18:37
 */
public class StringTypeHandler extends BaseTypeHandler<String>{
    @Override
    protected void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter);
    }
}