package org.bcliao.ibatis.mapping;

import org.bcliao.ibatis.scripting.LanguageDriver;
import org.bcliao.ibatis.session.Configuration;


/**
 *
 * 映射语句类
 *
 * @author: bcliao
 * @date: 2022/8/8 11:13
 **/
public class MappedStatement {

    private Configuration configuration;
    private String id;
    private SqlCommandType sqlCommandType;
    private SqlSource sqlSource;
    Class<?> resultType;
    private LanguageDriver lang;

    MappedStatement(){
        // constructor disabled
    }

    /**
     * 静态内部类 建造者模式
     */
    public static class Builder {

        private MappedStatement mappedStatement = new MappedStatement();

        public Builder(Configuration configuration,
                       String id,
                       SqlCommandType sqlCommandType,
                       SqlSource sqlSource,
                       Class<?> resultType){
            mappedStatement.configuration = configuration;
            mappedStatement.id = id;
            mappedStatement.sqlCommandType = sqlCommandType;
            mappedStatement.sqlSource = sqlSource;
            mappedStatement.resultType = resultType;
            mappedStatement.lang = configuration.getDefaultScriptingLanguageInstance();
        }

        public MappedStatement build(){
            assert mappedStatement.configuration != null;
            assert mappedStatement.id != null;
            return mappedStatement;
        }

    }


    public Configuration getConfiguration() {
        return configuration;
    }

    public String getId() {
        return id;
    }

    public SqlCommandType getSqlCommandType() {
        return sqlCommandType;
    }

    public SqlSource getSqlSource() {
        return sqlSource;
    }

    public Class<?> getResultType() {
        return resultType;
    }

    public LanguageDriver getLang() {
        return lang;
    }
}