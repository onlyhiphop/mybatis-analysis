### Mybatis整体架构

My Batis 分为三层

- 接口层
- 核心处理层 

- 基础支持层 

![image-20220805142405799](img/image-20220805142405799.png)



这里我们先来说说接口层：

​	接口层的和弦是 SqlSession 接口，该接口定义了 Mybatis 暴露给应用程序调用的 Api ，也就是上层应用与 Mybatis 交互的桥梁。接口层在接收到调用请求时，会调用核心处理层的相应模块来完成具体的数据库操作。



#### 流程

自动扫描包下接口并把每个接口类映射的代理类全部存入映射器代理的 HashMap 缓存中。通过调用 SqlSessionFactory 创建 SqlSession，再通过 SqlSession 获取注册机中的 Mapper 对象

![img](img/mybatis-220404-01.png)



#### 设计

面向接口编程，以抽象的基准去编写架构

- 建立统一的API交互接口  SqlSession 
- SqlSession 要被多次创建调用，使用工厂模式将 SqlSession 对象的创建集中管理，不再使用 new 去创建，将对象的创建和调用解耦，万一创建过程有修改，只需要在工厂集中修改。
- 建立工厂接口 SqlSessionFactory （同样的面向接口 便于后续不同工厂扩展）
- 创建映射器注册机负责注册缓存所有的的 映射器 Mapper



























