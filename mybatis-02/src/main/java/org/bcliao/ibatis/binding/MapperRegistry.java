package org.bcliao.ibatis.binding;

import cn.hutool.core.lang.ClassScanner;
import org.bcliao.ibatis.session.SqlSession;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * 映射器注册机
 *
 * @author: bcliao
 * @date: 2022/8/5 15:31
 **/
public class MapperRegistry {

    /**
     * 将已经添加的映射都放入 HashMap
     */
    private final Map<Class<?>, MapperProxyFactory<?>> knownMappers = new HashMap<>();

    public Map<Class<?>, MapperProxyFactory<?>> getKnownMappers() {
        return knownMappers;
    }

    /**
     * 这里入参要传 SqlSession ，是因为如果缓存中存的只是 ProxyFactory ，创建代理类需要用到 SqlSession
     * @param type
     * @param sqlSession
     * @param <T>
     * @return
     */
    public <T> T getMapper(Class<T> type, SqlSession sqlSession){
        final MapperProxyFactory<T> mapperProxyFactory = (MapperProxyFactory<T>) knownMappers.get(type);
        if(mapperProxyFactory == null){
            throw new RuntimeException("Type " + type + " is not known to the MapperRegistry.");
        }
        try {
            return mapperProxyFactory.newInstance(sqlSession);
        }catch (Exception e){
            throw new RuntimeException("Error getting mapper instance. Cause: " + e, e);
        }
    }

    public <T> void addMapper(Class<T> type){
        // Mapper 必须是接口才会注册
        if(type.isInterface()){
            if(hasMapper(type)){
                //如果重复添加，报错
                throw new RuntimeException("Type " + type + " is already known to the MapperRegistry.");
            }
            // 注册映射器代理工厂
            knownMappers.put(type, new MapperProxyFactory<>(type));
        }
    }

    /**
     * 添加包下所有类
     */
    public void addMappers(String packageName){
        //hutoll工具包
        Set<Class<?>> mapperSet = ClassScanner.scanPackage(packageName);
        for (Class<?> mapperClass : mapperSet) {
            addMapper(mapperClass);
        }
    }

    public <T> boolean hasMapper(Class<T> type) {
        return knownMappers.containsKey(type);
    }


}
