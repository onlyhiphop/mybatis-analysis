package org.bcliao.ibatis.session.defaults;

import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.binding.MapperRegistry;

/**
 * 默认SqlSession实现类
 * @author: bcliao
 * @date: 2022/8/5 14:50
 **/
public class DefaultSqlSession implements SqlSession {

    private MapperRegistry mapperRegistry;

    public DefaultSqlSession(MapperRegistry mapperRegistry) {
        this.mapperRegistry = mapperRegistry;
    }

    @Override
    public <T> T selectOne(String statement) {
        return (T) ("你被代理了！" + statement);
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        return (T) ("你被代理了！" + "方法：" + statement + " 入参：" + parameter);
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return mapperRegistry.getMapper(type, this);
    }
}
