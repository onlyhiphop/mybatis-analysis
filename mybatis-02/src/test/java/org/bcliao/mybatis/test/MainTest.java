package org.bcliao.mybatis.test;

import org.bcliao.ibatis.binding.MapperRegistry;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.defaults.DefaultSqlSessionFactory;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.junit.Test;

/**
 * @author: bcliao
 * @date: 2022/8/5 17:20
 **/
public class MainTest {

    @Test
    public void test_SqlSession(){

        // 1.注册Mapper
        MapperRegistry mapperRegistry = new MapperRegistry();
        mapperRegistry.addMappers("org.bcliao.mybatis.test.dao");

        // 2.从 SqlSession 工厂获取 SqlSession
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(mapperRegistry);
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        // 3.获取映射器对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        String res = userDao.queryUserName("001");
        System.out.println(res);

    }

}
