package org.bcliao.mybatis.test.dao;

/**
 * @author: bcliao
 * @date: 2022/8/5 17:20
 **/
public interface IUserDao {

    String queryUserName(String uId);

    Integer queryUserAge(String uId);

}
