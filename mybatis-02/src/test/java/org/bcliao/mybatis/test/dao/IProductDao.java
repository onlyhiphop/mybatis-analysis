package org.bcliao.mybatis.test.dao;

/**
 * @author: bcliao
 * @date: 2022/8/5 17:21
 **/
public interface IProductDao {

    String queryProductName(String pid);

}
