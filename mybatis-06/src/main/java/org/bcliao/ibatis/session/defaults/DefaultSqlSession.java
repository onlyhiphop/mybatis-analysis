package org.bcliao.ibatis.session.defaults;

import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.Environment;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.session.SqlSession;

import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 默认SqlSession实现类
 * @author: bcliao
 * @date: 2022/8/5 14:50
 **/
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;
    private Executor executor;

    public DefaultSqlSession(Configuration configuration, Executor executor) {
        this.configuration = configuration;
        this.executor = executor;
    }

    @Override
    public <T> T selectOne(String statement) {
        return this.selectOne(statement, null);
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        MappedStatement ms = configuration.getMappedStatement(statement);
        List<T> list = executor.query(ms, parameter, Executor.NO_RESULT_HANDLER, ms.getBoundSql());
        return list.get(0);
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return configuration.getMapper(type, this);
    }
}
