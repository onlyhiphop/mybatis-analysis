package org.bcliao.ibatis.datasource.pooled;

import org.bcliao.ibatis.datasource.unpooled.UnpooledDataSourceFactory;

import javax.sql.DataSource;

/**
 *
 * 有连接池的数据源工厂
 *
 * @author: bcliao
 * @date: 2022/8/15 10:34
 **/
public class PooledDataSourceFactory extends UnpooledDataSourceFactory {


    @Override
    public DataSource getDataSource() {
        PooledDataSource pooledDataSource = new PooledDataSource();
        pooledDataSource.setDriver(props.getProperty("driver"));
        pooledDataSource.setUrl(props.getProperty("url"));
        pooledDataSource.setUsername(props.getProperty("username"));
        pooledDataSource.setPassword(props.getProperty("password"));
        return pooledDataSource;
    }
}
