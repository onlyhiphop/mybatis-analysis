package org.bcliao.mybatis.test;

import com.alibaba.fastjson.JSON;
import org.bcliao.ibatis.datasource.pooled.PooledDataSource;
import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Test;

import java.sql.Connection;

/**
 * @author: bcliao
 * @date: 2022/8/15 13:50
 **/
public class MainTest {

    @Test
    public void test_SqlSessionFactory() throws Exception {
        // 1. 从 SqlSessionFactory 获取 SqlSession
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        // 2. 获取映射器对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        User user = userDao.queryUserInfoById(2);
        System.out.println(user);
    }



}
