package org.bcliao.ibatis.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * 拦截器链
 * 
 * @author bcliao
 * @date 2022/12/8 15:29
 */
public class InterceptorChain {
    
    private final List<Interceptor> interceptors = new ArrayList<>();
    
    public Object pluginAll(Object target){
        for (Interceptor interceptor : interceptors) {
            target = interceptor.plugin(target);
        }
        return target;
    }

    public void addInterceptor(Interceptor interceptorInstance) {
        interceptors.add(interceptorInstance);
    }
    
    public List<Interceptor> getInterceptors(){
        return Collections.unmodifiableList(interceptors);
    }
}
