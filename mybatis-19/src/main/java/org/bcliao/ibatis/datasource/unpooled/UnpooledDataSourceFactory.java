package org.bcliao.ibatis.datasource.unpooled;

import org.bcliao.ibatis.datasource.DataSourceFactory;
import org.bcliao.ibatis.reflection.MetaObject;
import org.bcliao.ibatis.reflection.SystemMetaObject;

import javax.sql.DataSource;
import java.util.Properties;

/**
 *
 * 无池化数据源工厂
 *
 * @author: bcliao
 * @date: 2022/8/12 15:21
 **/
public class UnpooledDataSourceFactory implements DataSourceFactory {

    protected DataSource dataSource;

    public UnpooledDataSourceFactory(){
        this.dataSource = new UnpooledDataSource();
    }

    @Override
    public void setProperties(Properties props) {
        // 获取 datasource 的元对象
        MetaObject metaObject = SystemMetaObject.forObject(dataSource);

        //这里遍历的是配置文件，拿着配置文件的属性去 set 进对象里
        for (Object key : props.keySet()) {
            String propertyName = (String) key;
            if(metaObject.hasSetter(propertyName)){
                String value = (String) props.get(propertyName);
                Object convertedValue = convertValue(metaObject, propertyName, value);
                metaObject.setValue(propertyName, convertedValue);
            }
        }
    }

    /**
     * 根据setter的类型，将配置文件中的值强转成相应的类型
     */
    private Object convertValue(MetaObject metaObject, String propertyName, String value) {
        Object convertedValue = value;
        Class<?> targetType = metaObject.getSetterType(propertyName);
        if(targetType == Integer.class || targetType == int.class){
            convertedValue = Integer.valueOf(value);
        }else if(targetType == Long.class || targetType == long.class){
            convertedValue = Long.valueOf(value);
        }else if(targetType == Boolean.class || targetType == boolean.class){
            convertedValue = Boolean.valueOf(value);
        }
        return convertedValue;
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }

}