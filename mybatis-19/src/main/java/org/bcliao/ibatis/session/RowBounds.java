package org.bcliao.ibatis.session;

/**
 * 
 * 分页记录限制
 * 它是逻辑分页：
 *  从数据库中拿出所有符合要求的数据，然后再从这些数据中拿到我们需要的分页数据。不适用于大批量和实时的数据
 * 
 * @Author bcliao
 * @Date 2022/9/26 13:35
 */
public class RowBounds {
    
    public static final int NO_ROW_OFFSET = 0;
    public static final int NO_ROW_LIMIT = Integer.MAX_VALUE;
    public static final RowBounds DEFAULT = new RowBounds();
    
    // offset , limit 就等于一般分页的 start , limit
    private int offset;
    private int limit;
    
    // 默认是一页 Integer.MAX_VALUE 条
    public RowBounds(){
        this.offset = NO_ROW_OFFSET;
        this.limit = NO_ROW_LIMIT;
    }

    public RowBounds(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

}