package org.bcliao.ibatis.cache;

import org.bcliao.ibatis.cache.decorators.TransactionalCache;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 事务缓存管理器，被 CachingExecutor 使用
 * 
 * @author bcliao
 * @date 2023/1/5 11:37
 */
public class TransactionalCacheManager {
    
    // 管理了许多 TransactionalCache
    private Map<Cache, TransactionalCache> transactionalCaches = new HashMap<>();
    
    public void clear(Cache cache){
        getTransactionalCache(cache).clear();
    }

    /**
     * 得到某个 TransactionalCache 的值
     */
    public Object getObject(Cache cache, CacheKey key){
        return getTransactionalCache(cache).getObject(key);
    }
    
    public void putObject(Cache cache, CacheKey key, Object value){
        getTransactionalCache(cache).putObject(key, value);
    }

    /**
     * 提交时全部提交
     */
    public void commit(){
        for (TransactionalCache txCache : transactionalCaches.values()) {
            txCache.commit();
        }
    }

    /**
     * 回滚时全部回滚
     */
    public void rollback(){
        for (TransactionalCache txCache : transactionalCaches.values()) {
            txCache.rollback();
        }
    }
    
    private TransactionalCache getTransactionalCache(Cache cache){
        TransactionalCache txCache = transactionalCaches.get(cache);
        if(txCache == null){
            txCache = new TransactionalCache(cache);
            transactionalCaches.put(cache, txCache);
        }
        return txCache;
    }

}
