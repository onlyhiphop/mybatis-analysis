package org.bcliao.ibatis.cache.decorators;

import org.bcliao.ibatis.cache.Cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * 事务缓存：
 *  一次性存入多个缓存，移除多个缓存
 * 
 * @author bcliao
 * @date 2023/1/5 11:06
 */
public class TransactionalCache implements Cache {
    
    private Cache delegate;
    // commit 时要不要清缓存
    private boolean clearOnCommit;
    // commit 时要添加的元素
    private Map<Object, Object> entriesToAddOnCommit;
    private Set<Object> entriesMissedInCache;

    public TransactionalCache(Cache delegate) {
        this.delegate = delegate;
        // 默认 commit 时不清缓存
        this.clearOnCommit = false;
        this.entriesToAddOnCommit = new HashMap<>();
        this.entriesMissedInCache = new HashSet<>();
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public void putObject(Object key, Object value) {
        entriesToAddOnCommit.put(key, value);
    }

    @Override
    public Object getObject(Object key) {
        // key: CacheKey 拼装后的哈希码
        Object object = delegate.getObject(key);
        if(object == null){
            entriesMissedInCache.add(key);
        }
        return clearOnCommit ? null : object;
    }

    @Override
    public Object removeObject(Object key) {
        return null;
    }

    @Override
    public void clear() {
        clearOnCommit = true;
        entriesToAddOnCommit.clear();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }
    
    // 多了 commit 方法，提供事务功能
    public void commit(){
        if(clearOnCommit){
            delegate.clear();
        }
        flushPendingEntries();
        reset();
    }

    public void rollback(){
        unlockMissedEntries();
        reset();
    }

    private void reset() {
        clearOnCommit = false;
        entriesToAddOnCommit.clear();
        entriesMissedInCache.clear();
    }

    private void flushPendingEntries() {

        for (Map.Entry<Object, Object> entry : entriesToAddOnCommit.entrySet()) {
            delegate.putObject(entry.getKey(), entry.getValue());
        }

        for (Object entry : entriesMissedInCache) {
            if(!entriesToAddOnCommit.containsKey(entry)){
                delegate.putObject(entry, null);
            }
        }
        
    }
    
    private void unlockMissedEntries(){

        for (Object entry : entriesMissedInCache) {
            delegate.putObject(entry, null);
        }
        
    }

}
