package org.bcliao.ibatis.reflection.invoker;

/**
 *
 * 调用者
 * 关于对象类中的属性值获取和设置可以分为 Field 字段的 get/set 还有普通的 Method 的调用，为了减少使用方的过多的处理，
 * 这里可以把集中调用者的实现包装成调用策略，统一接口不同策略不同的实现类。
 * 无论任何类型的反射调用，都离不开对象和入参，只要我们把这两个字段和返回结果定义的通用，就可以包住不同策略的实现类了
 *
 * @author: bcliao
 * @date: 2022/8/17 10:19
 **/
public interface Invoker {

    Object invoke(Object target, Object[] args) throws Exception;

    Class<?> getType();
}
