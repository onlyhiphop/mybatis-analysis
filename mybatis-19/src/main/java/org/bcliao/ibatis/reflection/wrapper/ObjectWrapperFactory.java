package org.bcliao.ibatis.reflection.wrapper;

import org.bcliao.ibatis.reflection.MetaObject;

/**
 *
 * 对象包装工厂
 *
 * @author: bcliao
 * @date: 2022/8/18 15:55
 **/
public interface ObjectWrapperFactory {

    /**
     * 判断有没有包装器
     * @param object
     * @return
     */
    boolean hasWrapperFor(Object object);

    /**
     * 得到包装器
     * @param metaObject
     * @param object
     * @return
     */
    ObjectWrapper getWrapperFor(MetaObject metaObject, Object object);
}
