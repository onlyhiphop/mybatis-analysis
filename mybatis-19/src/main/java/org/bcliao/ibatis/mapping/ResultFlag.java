package org.bcliao.ibatis.mapping;

/**
 * @author bcliao
 * @date 2022/11/28 14:42
 */
public enum ResultFlag {
    
    ID,
    CONSTRUCTOR
}
