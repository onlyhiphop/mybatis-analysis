package org.bcliao.ibatis.builder;

import org.bcliao.ibatis.mapping.ResultFlag;
import org.bcliao.ibatis.mapping.ResultMapping;
import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.type.TypeAliasRegistry;
import org.bcliao.ibatis.type.TypeHandler;
import org.bcliao.ibatis.type.TypeHandlerRegistry;

import java.util.List;

/**
 *
 * 构建器的基类，建造者模式
 *
 * @author: bcliao
 * @date: 2022/8/8 09:35
 **/
public abstract class BaseBuilder {

    protected final Configuration configuration;
    protected final TypeAliasRegistry typeAliasRegistry;
    protected final TypeHandlerRegistry typeHandlerRegistry;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        this.typeAliasRegistry = this.configuration.getTypeAliasRegistry();
        this.typeHandlerRegistry = this.configuration.getTypeHandlerRegistry();
    }

    public Configuration getConfiguration() {
        return configuration;
    }
    
    protected Class<?> resolveAlias(String alias){
        return typeAliasRegistry.resolveAlias(alias);
    }

    // 根据别名解析 Class 类型别名 注册 / 事务管理器别名
    protected Class<?> resolveClass(String alias){
        if(alias == null){
            return null;
        }
        try {
            return resolveAlias(alias);
        } catch (Exception e){
            throw new RuntimeException("Error resolving class. Cause: " + e, e);
        }
    }
    
    protected TypeHandler<?> resolveTypeHandler(Class<?> javaType, Class<? extends TypeHandler<?>> typeHandler){
        if(typeHandler == null){
            return null;
        }
        return typeHandlerRegistry.getMappingTypeHandler(typeHandler);
    }

    protected Boolean booleanValueOf(String value, Boolean defaultValue){
        return value == null ? defaultValue : Boolean.valueOf(value);
    }
}