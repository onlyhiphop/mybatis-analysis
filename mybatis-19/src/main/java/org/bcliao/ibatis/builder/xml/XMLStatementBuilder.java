package org.bcliao.ibatis.builder.xml;

import org.bcliao.ibatis.builder.BaseBuilder;
import org.bcliao.ibatis.builder.MapperBuilderAssistant;
import org.bcliao.ibatis.executor.keygen.Jdbc3KeyGenerator;
import org.bcliao.ibatis.executor.keygen.KeyGenerator;
import org.bcliao.ibatis.executor.keygen.NoKeyGenerator;
import org.bcliao.ibatis.executor.keygen.SelectKeyGenerator;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.mapping.SqlCommandType;
import org.bcliao.ibatis.mapping.SqlSource;
import org.bcliao.ibatis.scripting.LanguageDriver;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Element;

import java.util.List;
import java.util.Locale;

/**
 * 
 * XML 语句构建器
 * 
 * @Author bcliao
 * @Date 2022/8/31 17:35
 */
public class XMLStatementBuilder extends BaseBuilder {
    
    private MapperBuilderAssistant builderAssistant;
    private Element element;

    public XMLStatementBuilder(Configuration configuration, MapperBuilderAssistant builderAssistant, Element element) {
        super(configuration);
        this.builderAssistant = builderAssistant;
        this.element = element;
    }

    //解析语句(select|insert|update|delete)

    /**
     * <select
     *   id="selectPerson"
     *   parameterType="int"
     *   parameterMap="deprecated"
     *   resultType="hashmap"
     *   resultMap="personResultMap"
     *   flushCache="false"
     *   useCache="true"
     *   timeout="10000"
     *   fetchSize="256"
     *   statementType="PREPARED"
     *   resultSetType="FORWARD_ONLY">
     *   
     *   SELECT * FROM PERSON WHERE ID = #{id}
     * </select>
     */
    public void parseStatementNode() {
        String id = element.attributeValue("id");
        // 参数类型
        String parameterType = element.attributeValue("parameterType");
        Class<?> parameterTypeClass = resolveAlias(parameterType);
        // 外部应用 resultMap
        String resultMap = element.attributeValue("resultMap");
        // 结果类型
        String resultType = element.attributeValue("resultType");
        Class<?> resultTypeClass = resolveAlias(resultType);
        // 获取命令类型(select | insert | update | delete)
        String nodeName = element.getName();
        SqlCommandType sqlCommandType = SqlCommandType.valueOf(nodeName.toUpperCase(Locale.ENGLISH));
        
        boolean isSelect = sqlCommandType == SqlCommandType.SELECT;
        boolean flushCache = Boolean.parseBoolean(element.attributeValue("flushCache", String.valueOf(!isSelect)));
        boolean useCache = Boolean.parseBoolean(element.attributeValue("useCache", String.valueOf(!isSelect)));
        
        // 获取默认语言驱动器    
        Class<?> langClass = configuration.getLanguageRegistry().getDefaultDriverClass();
        LanguageDriver languageDriver = configuration.getLanguageRegistry().getDriver(langClass);

        // 解析 <selectKey>
        processSelectKeyNodes(id, parameterTypeClass, languageDriver);
        
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, element, parameterTypeClass);

        // 属性标记【仅对 insert 有用】， MyBatis 会通过 getGeneratedKeys 或者通过 insert 语句的 selectKey 子元素设置它的值
        String keyProperty = element.attributeValue("keyProperty");

        KeyGenerator keyGenerator = null;
        String keyStatementId = id + SelectKeyGenerator.SELECT_KEY_SUFFIX;
        keyStatementId = builderAssistant.applyCurrentNamespace(keyStatementId, true);
        
        if(configuration.hasKeyGenerator(keyStatementId)){
            keyGenerator = configuration.getKeyGenerator(keyStatementId);
        } else {
            keyGenerator = configuration.isUseGeneratedKeys() && SqlCommandType.INSERT.equals(sqlCommandType) ? new Jdbc3KeyGenerator() : new NoKeyGenerator();
        }

        // 调用助手类【本节新添加，便于统一处理参数的包装】
        //作用就是往configuration中添加mappedStatement
        builderAssistant.addMappedStatement(id,
                sqlSource,
                sqlCommandType,
                parameterTypeClass,
                resultMap,
                resultTypeClass,
                flushCache,
                useCache,
                keyGenerator,
                keyProperty,
                languageDriver);
    }

    private void processSelectKeyNodes(String id, Class<?> parameterTypeClass, LanguageDriver languageDriver) {
        List<Element> selectKeyNodes = element.elements("selectKey");
        parseSelectKeyNodes(id, selectKeyNodes, parameterTypeClass, languageDriver);
    }

    private void parseSelectKeyNodes(String parentId, List<Element> selectKeyNodes, Class<?> parameterTypeClass, LanguageDriver languageDriver) {
        for (Element nodeToHandle : selectKeyNodes) {
            String id = parentId + SelectKeyGenerator.SELECT_KEY_SUFFIX;
            parseSelectKeyNode(id, nodeToHandle, parameterTypeClass, languageDriver);
        }
    }

    /**
     * <selectKey keyProperty="id" order="AFTER" resultType="long">
     * SELECT LAST_INSERT_ID()
     * </selectKey>
     */
    private void parseSelectKeyNode(String id, Element nodeToHandle, Class<?> parameterTypeClass, LanguageDriver languageDriver){
        String resultType = nodeToHandle.attributeValue("resultType");
        Class<?> resultTypeClass = resolveClass(resultType);
        boolean executeBefore = "BEFORE".equals(nodeToHandle.attributeValue("order", "AFTER"));
        String keyProperty = nodeToHandle.attributeValue("keyProperty");
        
        //default
        String resultMap = null;
        boolean flushCache = false;
        boolean useCache = false;
        KeyGenerator keyGenerator = new NoKeyGenerator();
        
        //解析成 SqlSource, DynamicSqlSource / RawSqlSource
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, nodeToHandle, parameterTypeClass);
        SqlCommandType sqlCommandType = SqlCommandType.SELECT;
        
        // 调用助手类
        builderAssistant.addMappedStatement(id,
                sqlSource,
                sqlCommandType,
                parameterTypeClass,
                resultMap,
                resultTypeClass,
                flushCache,
                useCache,
                keyGenerator,
                keyProperty,
                languageDriver);
        
        // 给 id 加上 namespace 前缀
        id = builderAssistant.applyCurrentNamespace(id, false);
        
        // 存放键值生成器配置
        MappedStatement keyStatement = configuration.getMappedStatement(id);
        configuration.addKeyGenerator(id, new SelectKeyGenerator(keyStatement, executeBefore));
    }
}