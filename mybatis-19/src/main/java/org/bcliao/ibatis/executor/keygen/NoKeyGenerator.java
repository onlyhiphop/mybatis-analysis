package org.bcliao.ibatis.executor.keygen;

import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.mapping.MappedStatement;

import java.sql.Statement;

/**
 * 
 * 不用键值生成器
 * 
 * @author bcliao
 * @date 2022/11/30 14:38
 */
public class NoKeyGenerator implements KeyGenerator{
    @Override
    public void processBefore(Executor executor, MappedStatement ms, Statement stmt, Object parameter) {
        // Do Nothing
    }

    @Override
    public void processAfter(Executor executor, MappedStatement ms, Statement stmt, Object parameter) {
        // Do Nothing
    }
}
