

如果说我们需要对一个对象的所提供的属性进行统一的设置和获取值的操作，那么就需要把当前这个被处理的对象进行解耦，提取出它所有的属性和方法，并按照不同的类型进行反射处理，从而包装成一个工具包

![图 8-2 对象属性反射处理](img/mybatis-220506-02.png)

- 其实整个设计过程都以围绕如何拆解对象并提供反射操作为主，那么对于一个对象来说，它所包括的有对象的构造函数、对象的属性、对象的方法。而对象的方法因为都是获取和设置值的操作，所以基本都是get、set处理，所以需要把这些方法在对象拆解的过程中需要摘取出来进行保存。
- 当真正的开始操作时，则会依赖于已经实例化的对象，对其进行属性处理。而这些处理过程实际都是在使用 JDK 所提供的反射进行操作的，而反射过程中的方法名称、入参类型都已经被我们拆解和处理了，最终使用的时候直接调用即可。



元对象反射工具类，处理对象的属性设置和获取操作核心类：

![图 8-3 所示 元对象反射工具类，处理对象的属性设置和获取操作核心类](img/mybatis-220506-03.png)

- 以 Reflector 反射器类处理对象类中的 get/set 属性，包装为可调用的 Invoker 反射类，这样在对 get/set 方法反射调用的时候，使用方法名称获取对应的 Invoker 即可 `getGetInvoker(String propertyName)`。
- 有了反射器的处理，之后就是对原对象的包装了，由 SystemMetaObject 提供创建 MetaObject 元对象的方法，将我们需要处理的对象进行拆解和 ObjectWrapper 对象包装处理。因为一个对象的类型还需要进行一条细节的处理，以及属性信息的拆解，例如：`班级[0].学生.成绩` 这样一个类中的关联类的属性，则需要进行递归的方式拆解处理后，才能设置和获取属性值。
- 最终在 Mybatis 其他的地方就可以，有需要属性值设定时，就可以使用到反射工具包进行处理了。这里首当其冲的我们会把数据源池化中关于 Properties 属性的处理使用反射工具类进行改造。