package org.bcliao.ibatis.session;

import org.bcliao.ibatis.binding.MapperRegistry;
import org.bcliao.ibatis.datasource.druid.DruidDataSourceFactory;
import org.bcliao.ibatis.datasource.pooled.PooledDataSourceFactory;
import org.bcliao.ibatis.datasource.unpooled.UnpooledDataSourceFactory;
import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.executor.SimpleExecutor;
import org.bcliao.ibatis.executor.resultset.DefaultResultSetHandler;
import org.bcliao.ibatis.executor.resultset.ResultSetHandler;
import org.bcliao.ibatis.executor.statement.PreparedStatementHandler;
import org.bcliao.ibatis.executor.statement.StatementHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.Environment;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.transaction.Transaction;
import org.bcliao.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.bcliao.ibatis.type.TypeAliasRegistry;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 这里是所有 mapper 接口 (xml文件) 对应一个 configuration
 *
 * @author: bcliao
 * @date: 2022/8/8 09:39
 **/
public class Configuration {

    /** 环境 */
    protected Environment environment;
    /** 映射注册机 */
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);
    /** 类型别名注册机 */
    protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry();

    /**
     * 映射的语句， 存在 Map 里
     * key: 接口方法的全路径名称（含包名）
     */
    protected final Map<String, MappedStatement> mappedStatements = new HashMap<>();

    public Configuration(){
        typeAliasRegistry.registerAlias("JDBC", JdbcTransactionFactory.class);
        typeAliasRegistry.registerAlias("DRUID", DruidDataSourceFactory.class);
        typeAliasRegistry.registerAlias("UNPOOLED", UnpooledDataSourceFactory.class);
        typeAliasRegistry.registerAlias("POOLED", PooledDataSourceFactory.class);
    }

    public void addMappers(String packageName){
        mapperRegistry.addMappers(packageName);
    }

    public <T> void addMapper(Class<T> type){
        mapperRegistry.addMapper(type);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession){
        return mapperRegistry.getMapper(type, sqlSession);
    }

    public boolean hasMapper(Class<?> type){
        return mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms){
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(String id){
        return mappedStatements.get(id);
    }

    public TypeAliasRegistry getTypeAliasRegistry() {
        return typeAliasRegistry;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    /**
     * 创建结果集处理器
     * @return
     */
    public ResultSetHandler newResultSetHandler(Executor executor, MappedStatement mappedStatement, BoundSql boundSql){
        return new DefaultResultSetHandler(executor, mappedStatement, boundSql);
    }

    /**
     * 产生执行器
     */
    public Executor newExecutor(Transaction transaction){
        return new SimpleExecutor(this, transaction);
    }

    /**
     * 创建语句处理器
     */
    public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameter, ResultHandler resultHandler, BoundSql boundSql) {
        return new PreparedStatementHandler(executor, mappedStatement, parameter, resultHandler, boundSql);
    }
}
