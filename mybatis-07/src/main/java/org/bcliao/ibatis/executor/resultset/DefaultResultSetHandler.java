package org.bcliao.ibatis.executor.resultset;

import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;

import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * 默认 Map 结果处理器
 *
 * @author: bcliao
 * @date: 2022/8/16 09:28
 **/
public class DefaultResultSetHandler implements ResultSetHandler{

    private final BoundSql boundSql;

    public DefaultResultSetHandler(Executor executor, MappedStatement mappedStatement, BoundSql boundSql){
        this.boundSql = boundSql;
    }

    @Override
    public <E> List<E> handleResultSets(Statement stmt) throws SQLException {
        ResultSet resultSet = stmt.getResultSet();

        try {
            return resultSet2Obj(resultSet, Class.forName(boundSql.getResultType()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private <T> List<T> resultSet2Obj(ResultSet resultSet, Class<?> clazz) {
        List<T> list = new ArrayList<>();

        try {

            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            //遍历行值
            while(resultSet.next()){
                T obj = (T) clazz.newInstance();
                for (int i = 1; i <= columnCount; i++) {
                    Object value = resultSet.getObject(i);
                    String columnLabel = metaData.getColumnLabel(i);
                    String setMethod = "set" + columnLabel.substring(0, 1).toUpperCase() + columnLabel.substring(1);
                    Method method;
                    if(value instanceof Timestamp){
                        method = clazz.getMethod(setMethod, Date.class);
                    }else{
                        method = clazz.getMethod(setMethod, value.getClass());
                    }
                    method.invoke(obj, value);
                }
                list.add(obj);
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return list;
    }

}
