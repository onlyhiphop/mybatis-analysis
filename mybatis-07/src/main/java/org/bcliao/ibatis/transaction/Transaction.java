package org.bcliao.ibatis.transaction;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * 事务接口
 *
 * @author: bcliao
 * @date: 2022/8/10 10:40
 **/
public interface Transaction {

    Connection getConnection() throws SQLException;

    void commit() throws SQLException;

    void rollback() throws SQLException;

    void close() throws SQLException;

}
