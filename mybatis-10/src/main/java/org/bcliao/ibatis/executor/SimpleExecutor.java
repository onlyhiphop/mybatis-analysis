package org.bcliao.ibatis.executor;

import org.bcliao.ibatis.executor.statement.StatementHandler;
import org.bcliao.ibatis.mapping.BoundSql;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.session.ResultHandler;
import org.bcliao.ibatis.session.RowBounds;
import org.bcliao.ibatis.transaction.Transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 *
 *  简单执行器
 *
 * @author: bcliao
 * @date: 2022/8/15 18:47
 **/
public class SimpleExecutor extends BaseExecutor {

    public SimpleExecutor(Configuration configuration, Transaction transaction){
        super(configuration, transaction);
    }

    @Override
    protected <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        try {
            Configuration configuration = ms.getConfiguration();
            // 新建一个 StatementHandler
            StatementHandler handler = configuration.newStatementHandler(this, ms, parameter, rowBounds, resultHandler, boundSql);
            Connection connection = transaction.getConnection();
            // 准备语句
            Statement stmt = handler.prepare(connection);
            handler.parameterize(stmt);
            // 返回结果
            return handler.query(stmt, resultHandler);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
