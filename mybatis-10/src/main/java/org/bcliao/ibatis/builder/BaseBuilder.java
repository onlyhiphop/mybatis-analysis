package org.bcliao.ibatis.builder;

import org.bcliao.ibatis.session.Configuration;
import org.bcliao.ibatis.type.TypeAliasRegistry;
import org.bcliao.ibatis.type.TypeHandlerRegistry;

/**
 *
 * 构建器的基类，建造者模式
 *
 * @author: bcliao
 * @date: 2022/8/8 09:35
 **/
public abstract class BaseBuilder {

    protected final Configuration configuration;
    protected final TypeAliasRegistry typeAliasRegistry;
    protected final TypeHandlerRegistry typeHandlerRegistry;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        this.typeAliasRegistry = this.configuration.getTypeAliasRegistry();
        this.typeHandlerRegistry = this.configuration.getTypeHandlerRegistry();
    }

    public Configuration getConfiguration() {
        return configuration;
    }
    
    protected Class<?> resolveAlias(String alias){
        return typeAliasRegistry.resolveAlias(alias);
    }

}