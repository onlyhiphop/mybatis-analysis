package org.bcliao.ibatis.builder.xml;

import org.bcliao.ibatis.builder.BaseBuilder;
import org.bcliao.ibatis.builder.MapperBuilderAssistant;
import org.bcliao.ibatis.mapping.MappedStatement;
import org.bcliao.ibatis.mapping.SqlCommandType;
import org.bcliao.ibatis.mapping.SqlSource;
import org.bcliao.ibatis.scripting.LanguageDriver;
import org.bcliao.ibatis.session.Configuration;
import org.dom4j.Element;

import java.util.Locale;

/**
 * 
 * XML 语句构建器
 * 
 * @Author bcliao
 * @Date 2022/8/31 17:35
 */
public class XMLStatementBuilder extends BaseBuilder {
    
    private MapperBuilderAssistant builderAssistant;
    private Element element;

    public XMLStatementBuilder(Configuration configuration, MapperBuilderAssistant builderAssistant, Element element) {
        super(configuration);
        this.builderAssistant = builderAssistant;
        this.element = element;
    }

    //解析语句(select|insert|update|delete)

    /**
     * <select
     *   id="selectPerson"
     *   parameterType="int"
     *   parameterMap="deprecated"
     *   resultType="hashmap"
     *   resultMap="personResultMap"
     *   flushCache="false"
     *   useCache="true"
     *   timeout="10000"
     *   fetchSize="256"
     *   statementType="PREPARED"
     *   resultSetType="FORWARD_ONLY">
     *   
     *   SELECT * FROM PERSON WHERE ID = #{id}
     * </select>
     */
    public void parseStatementNode() {
        String id = element.attributeValue("id");
        // 参数类型
        String parameterType = element.attributeValue("parameterType");
        Class<?> parameterTypeClass = resolveAlias(parameterType);
        // 外部应用 resultMap
        String resultMap = element.attributeValue("resultMap");
        // 结果类型
        String resultType = element.attributeValue("resultType");
        Class<?> resultTypeClass = resolveAlias(resultType);
        // 获取命令类型(select | insert | update | delete)
        String nodeName = element.getName();
        SqlCommandType sqlCommandType = SqlCommandType.valueOf(nodeName.toUpperCase(Locale.ENGLISH));
        
        // 获取默认语言驱动器    
        Class<?> langClass = configuration.getLanguageRegistry().getDefaultDriverClass();
        LanguageDriver languageDriver = configuration.getLanguageRegistry().getDriver(langClass);

        SqlSource sqlSource = languageDriver.createSqlSource(configuration, element, parameterTypeClass);

        // 调用助手类【本节新添加，便于统一处理参数的包装】
        //作用就是往configuration中添加mappedStatement
        builderAssistant.addMappedStatement(id,
                sqlSource,
                sqlCommandType,
                parameterTypeClass,
                resultMap,
                resultTypeClass,
                languageDriver);
    }
}