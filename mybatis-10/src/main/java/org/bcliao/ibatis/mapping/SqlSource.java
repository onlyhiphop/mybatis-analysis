package org.bcliao.ibatis.mapping;

/**
 * 
 * SQL 源码
 * 
 * @Author bcliao
 * @Date 2022/9/1 17:33
 */
public interface SqlSource {
    
    BoundSql getBoundSql(Object parameterObject);
    
}