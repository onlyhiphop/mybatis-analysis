#### 封装结果集处理器

在我们使用 JDBC 获取到查询结果 ResultSet#getObject 可以获取返回属性值，但其实 ResultSet 是可以按照不同的属性类型进行返回结果的，而不是都返回 Object 对象。那么其实我们在上一章节中处理属性信息时候，所开发的 TypeHandler 接口的实现类，就可以扩充返回结果的方法，例如：LongTypeHandler#getResult、StringTypeHandler#getResult 等，这样我们就可以使用策略模式非常明确的定位到返回的结果，而不需要进行if判断处理。

再有了这个目标的前提下，就可以通过解析 XML 信息时封装返回类型到映射器语句类中，MappedStatement#resultMaps 直到执行完 SQL 语句，按照我们的返回结果参数类型，创建对象和使用 MetaObject 反射工具类填充属性信息。

![图 11-3 封装结果集处理器](img/mybatis-220602-03.png)

- 首先我们在解析 XML 语句解析构建器中，添加一个 MapperBuilderAssistant 映射器的助手类，方便我们对参数的统一包装处理，按照职责归属的方式进行细分解耦。通过这样的方式在 MapperBuilderAssistant#setStatementResultMap 中封装返回结果信息，一般来说我们使用 Mybatis 配置返回对象的时候 ResultType 就能解决大部分问题，而不需要都是配置一个 ResultMap 映射结果。但这里的设计其实是把 ResultType 也按照一个 ResultMap 的方式进行封装处理，这样统一一个标准的方式进行包装，做了到适配的效果，也更加方便后面对这样的参数进行统一使用。
- 接下来就是执行 JDBC 操作查询到数据以后，对结果的封装。那么在 DefaultResultSetHandler 返回结果处理中，首先会按照我们已经解析的到的 ResultType 进行对象的实例化。实例化对象以后再根据解析出来对象中参数的名称获取对应的类型，在根据类型找到 TypeHandler 接口实现类，也就是我们前面提到的 LongTypeHandler、StringTypeHandler，因为通过这样的方式，可以避免 if···else 的判断，而是直接O(1)时间复杂度定位到对应的类型处理器，在不同的类型处理器中返回结果信息。最终拿到结果再通过前面章节已经开发过的 MetaObject 反射工具类进行属性信息的设置。*metaObject.setValue(property, value)* 最终填充实例化并设置了属性内容的结果对象到上下文中，直至处理完成返回最终的结果数据，以此处理完成。



#### 总结

- 这一章节的整个功能实现，都在围绕流程的解耦进行处理，将对象的参数解析和结果封装都进行拆解，通过这样的方式来分配各个模块的单一职责，不让一个类的方法承担过多的交叉功能。
- 那么我们在结合这样的思想和设计，反复阅读和动手实践中，来学习这样的代码设计和开发过程，都能为我们以后实际开发业务代码时候带来参考建议，避免总是把所有的流程都写到一个类或者方法中。
- 到本章节全核心流程基本就串联清楚了，再有的就是一些功能的拓展，比如支持更多的参数类型，以及添加除了 Select 以外的其他操作，还有一些缓存数据的使用等，后面章节将在这些内容中，摘取一些核心的设计和实现进行讲解，让读者吸收更多的设计技巧