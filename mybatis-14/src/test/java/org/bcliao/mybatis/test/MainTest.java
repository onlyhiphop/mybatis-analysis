package org.bcliao.mybatis.test;

import org.bcliao.ibatis.io.Resources;
import org.bcliao.ibatis.session.SqlSession;
import org.bcliao.ibatis.session.SqlSessionFactory;
import org.bcliao.ibatis.session.SqlSessionFactoryBuilder;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.bcliao.mybatis.test.po.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

/**
 * @author bcliao
 * @date 2022/12/5 10:29
 */
public class MainTest {
    
    private SqlSession sqlSession;
    private IUserDao userDao;
    
    @Before
    public void init() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config-datasource.xml"));
        sqlSession = sqlSessionFactory.openSqlSession();
        
        userDao = sqlSession.getMapper(IUserDao.class);
    }
    
    @Test
    public void insert(){
        User user = new User();
        user.setName("测试插入1205");
        user.setAge(56);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        int count = userDao.insertUserInfo(user);
        System.out.println(user.getId());
    }
    
}
