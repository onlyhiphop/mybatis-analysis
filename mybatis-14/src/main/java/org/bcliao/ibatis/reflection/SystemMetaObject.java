package org.bcliao.ibatis.reflection;

import org.bcliao.ibatis.reflection.factory.DefaultObjectFactory;
import org.bcliao.ibatis.reflection.factory.ObjectFactory;
import org.bcliao.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.bcliao.ibatis.reflection.wrapper.ObjectWrapperFactory;

/**
 *
 * 一些系统级别的元对象
 *
 * @author: bcliao
 * @date: 2022/8/19 09:09
 **/
public class SystemMetaObject {

    public static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
    public static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
    public static final MetaObject NULL_META_OBJECT = MetaObject.forObject(NullObject.class, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY);

    private SystemMetaObject() {
        // Prevent Instantiation of Static Class
    }

    /**
     * 空对象
     */
    private static class NullObject {
    }

    public static MetaObject forObject(Object object) {
        return MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY);
    }

}
