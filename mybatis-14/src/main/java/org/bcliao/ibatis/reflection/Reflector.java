package org.bcliao.ibatis.reflection;

import org.bcliao.ibatis.reflection.invoker.GetFieldInvoker;
import org.bcliao.ibatis.reflection.invoker.Invoker;
import org.bcliao.ibatis.reflection.invoker.MethodInvoker;
import org.bcliao.ibatis.reflection.invoker.SetFieldInvoker;
import org.bcliao.ibatis.reflection.property.PropertyNamer;

import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * 一个对象对应一个Reflector
 * Reflector 反射器专门用于解耦对象信息的，只有把一个对象信息所含带的属性、方法以及关联的类都以此解析出来，才能满足后续对属性值的设置和获取。
 * Reflector 反射器类中提供了各类属性、方法、类型以及构造函数的保存操作，当调用反射器时会通过构造函数的处理，逐步从对象类中拆解出这些属性信息，便于后续反射使用。
 *
 * @author: bcliao
 * @date: 2022/8/17 10:34
 **/
public class Reflector {

    private static boolean classCacheEnabled = true;

    private static final String[] EMPTY_STRING_ARRAY = new String[0];

    // 线程安全的缓存
    private static final Map<Class<?>, Reflector> REFLECTOR_MAP = new ConcurrentHashMap<>();

    private Class<?> type;

    // get 属性列表
    private String[] readablePropertyNames = EMPTY_STRING_ARRAY;
    // set 属性列表
    private String[] writeablePropertyNames = EMPTY_STRING_ARRAY;
    // set 方法列表 key: 属性名称 value: MethodInvoker
    private Map<String, Invoker> setMethods = new HashMap<>();
    // get 方法列表 key: 属性名称 value: MethodInvoker
    private Map<String, Invoker> getMethods = new HashMap<>();
    // set 类型列表 key:属性名称 value: 参数类型
    private Map<String, Class<?>> setTypes = new HashMap<>();
    // get 类型列表 key:属性名称 value: 返回类型
    private Map<String, Class<?>> getTypes = new HashMap<>();
    // 构造函数
    private Constructor<?> defaultConstructor;

    // 不区分大小写的属性映射
    private Map<String, String> caseInsensitivePropertyMap = new HashMap<>();

    public Reflector(Class<?> clazz){
        this.type = clazz;
        // 加入构造函数
        addDefaultConstructor(clazz);
        // 加入 getter
        addGetMethods(clazz);
        // 加入 setter
        addSetMethods(clazz);
        // 加入字段
        addFields(clazz);
        readablePropertyNames = getMethods.keySet().toArray(new String[getMethods.keySet().size()]);
        writeablePropertyNames = setMethods.keySet().toArray(new String[setMethods.keySet().size()]);
        for (String propName : readablePropertyNames) {
            caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
        }
        for (String propName : writeablePropertyNames) {
            caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
        }
    }

    private void addDefaultConstructor(Class<?> clazz) {
        //返回所有的构造器，无论是 public 的还是 private 的，但是不包括从父类继承的
        Constructor<?>[] declaredConstructors = clazz.getDeclaredConstructors();
        for (Constructor<?> constructor : declaredConstructors) {
            //如果是无参构造
            if(constructor.getParameterTypes().length == 0){
                if(canAccessPrivateMethods()){
                    try {
                        //设置能够通过反射去访问私有构造方法
                        constructor.setAccessible(true);
                    } catch (Exception e) {
                        // Ignored. This is only a final precaution, nothing we can do.
                    }
                }
            }

            //循环最后的结果就是 defaultConstructor = 无参构造，因为默认 isAccessible 就是 false
            if(constructor.isAccessible()){
                this.defaultConstructor = constructor;
            }
        }
    }

    private void addGetMethods(Class<?> clazz) {
        // key: methodName
        Map<String, List<Method>> conflictingGetters = new HashMap<>();
        Method[] methods = getClassMethods(clazz);
        for (Method method : methods) {
            String name = method.getName();
            if(name.startsWith("get") && name.length() > 3){
                if(method.getParameterTypes().length == 0){
                    name = PropertyNamer.methodToProperty(name);
                    addMethodConflict(conflictingGetters, name, method);
                }
            } else if(name.startsWith("is") && name.length() > 2){
                if(method.getParameterTypes().length == 0){
                    name = PropertyNamer.methodToProperty(name);
                    addMethodConflict(conflictingGetters, name, method);
                }
            }
        }
        resolveGetterConflicts(conflictingGetters);
    }

    private void addGetMethod(String name, Method method) {
        if (isValidPropertyName(name)) {
            getMethods.put(name, new MethodInvoker(method));
            getTypes.put(name, method.getReturnType());
        }
    }

    private void addSetMethods(Class<?> clazz) {
        Map<String, List<Method>> conflictingSetters = new HashMap<>();
        Method[] methods = getClassMethods(clazz);
        for (Method method : methods) {
            String name = method.getName();
            if(name.startsWith("set") && name.length() > 3){
                if(method.getParameterTypes().length == 1){
                    name = PropertyNamer.methodToProperty(name);
                    addMethodConflict(conflictingSetters, name, method);
                }
            }
        }
        resolveSetterConflicts(conflictingSetters);
    }

    private void addSetMethod(String name, Method method) {
        if(isValidPropertyName(name)){
            setMethods.put(name, new MethodInvoker(method));
            setTypes.put(name, method.getParameterTypes()[0]);
        }
    }

    private void addFields(Class<?> clazz) {
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            if(canAccessPrivateMethods()){
                try {
                    field.setAccessible(true);
                } catch (Exception e) {
                    // Ignored. This is only a final precaution, nothing we can do.
                }
            }
            if(field.isAccessible()){
                // 该字段没有 set 方法
                if(!setMethods.containsKey(field.getName())){
                    // 返回字段的修饰符的 int 类型 ，如 public 为 1 ，static 为 8
                    int modifiers = field.getModifiers();
                    if(!(Modifier.isFinal(modifiers) && Modifier.isStatic(modifiers))){
                        addSetField(field);
                    }
                }
                // 该字段没有get方法
                if(!getMethods.containsKey(field.getName())){
                    addGetField(field);
                }
            }
        }
    }

    private void addGetField(Field field) {
        if(isValidPropertyName(field.getName())){
            getMethods.put(field.getName(), new GetFieldInvoker(field));
            getTypes.put(field.getName(), field.getType());
        }
    }

    private void addSetField(Field field) {
        if(isValidPropertyName(field.getName())){
            setMethods.put(field.getName(), new SetFieldInvoker(field));
            setTypes.put(field.getName(), field.getType());
        }
    }

    private void resolveSetterConflicts(Map<String, List<Method>> conflictingSetters) {
        for (String propName : conflictingSetters.keySet()) {
            List<Method> setters = conflictingSetters.get(propName);
            Method firstMethod = setters.get(0);
            if(setters.size() == 1){
                addSetMethod(propName, firstMethod);
            }else{
                Class<?> expectedType = getTypes.get(propName);
                if(expectedType == null){
                    throw new RuntimeException("Illegal overloaded setter method with ambiguous type for property "
                            + propName + " in class " + firstMethod.getDeclaringClass() + ".  This breaks the JavaBeans " +
                            "specification and can cause unpredicatble results.");
                }else{
                    Iterator<Method> methods = setters.iterator();
                    Method setter = null;
                    while(methods.hasNext()){
                        Method method = methods.next();
                        if(method.getParameterTypes().length == 1
                                && expectedType.equals(method.getParameterTypes()[0])){
                            setter = method;
                            break;
                        }
                    }
                    if(setter == null){
                        throw new RuntimeException("Illegal overloaded setter method with ambiguous type for property "
                                + propName + " in class " + firstMethod.getDeclaringClass() + ".  This breaks the JavaBeans " +
                                "specification and can cause unpredicatble results.");
                    }
                    addSetMethod(propName, setter);
                }
            }
        }
    }


    private boolean isValidPropertyName(String name) {
        return !(name.startsWith("$") || "serialVersionUID".equals(name) || ".class".equals(name));
    }

    private void resolveGetterConflicts(Map<String, List<Method>> conflictingGetters) {
        for (String propName : conflictingGetters.keySet()) {
            List<Method> getters = conflictingGetters.get(propName);
            Iterator<Method> iterator = getters.iterator();
            Method firstMethod = iterator.next();
            if(getters.size() == 1){
                addGetMethod(propName, firstMethod);
            }else{
                Method getter = firstMethod;
                Class<?> getterType = firstMethod.getReturnType();
                while(iterator.hasNext()){
                    Method method = iterator.next();
                    Class<?> methodType = method.getReturnType();
                    if (methodType.equals(getterType)) {
                        throw new RuntimeException("Illegal overloaded getter method with ambiguous type for property "
                                + propName + " in class " + firstMethod.getDeclaringClass()
                                + ".  This breaks the JavaBeans " + "specification and can cause unpredicatble results.");
                    }else if(methodType.isAssignableFrom(getterType)){
                        // 是 getterType 的父类
                    }else if(getterType.isAssignableFrom(methodType)){
                        getter = method;
                        getterType = methodType;
                    }else{
                        throw new RuntimeException("Illegal overloaded getter method with ambiguous type for property "
                                + propName + " in class " + firstMethod.getDeclaringClass()
                                + ".  This breaks the JavaBeans " + "specification and can cause unpredicatble results.");
                    }
                }
                addGetMethod(propName, getter);
            }
        }
    }

    private void addMethodConflict(Map<String, List<Method>> conflictingGetters, String name, Method method) {
        List<Method> list = conflictingGetters.computeIfAbsent(name, k -> new ArrayList<>());
        list.add(method);
    }

    /**
     * 去重得到所有的方法(private public ..) 包括父类、接口
     * @param clazz
     * @return
     */
    private Method[] getClassMethods(Class<?> clazz) {
        // 这个 map 用来存储然后去重
        Map<String, Method> uniqueMethods = new HashMap<>();
        Class<?> currentClass = clazz;
        while(currentClass != null){    // 循环调用得到父类

            addUniqueMethods(uniqueMethods, currentClass.getDeclaredMethods());

            Class<?>[] interfaces = currentClass.getInterfaces();
            for (Class<?> anInterface : interfaces) {
                // 得到所有接口的方法
                addUniqueMethods(uniqueMethods, anInterface.getMethods());
            }

            currentClass = currentClass.getSuperclass();
        }
        Collection<Method> methods = uniqueMethods.values();
        return methods.toArray(new Method[methods.size()]);
    }

    private void addUniqueMethods(Map<String, Method> uniqueMethods, Method[] declaredMethods) {
        for (Method currentMethod : declaredMethods) {
            if(!currentMethod.isBridge()){
                // 取得签名
                String signature = getSignature(currentMethod);
                if(!uniqueMethods.containsKey(signature)){
                    if(canAccessPrivateMethods()){
                        try {
                            currentMethod.setAccessible(true);
                        } catch (Exception e) {
                            // Ignored. This is only a final precaution, nothing we can do.
                        }
                    }

                    uniqueMethods.put(signature, currentMethod);
                }
            }
        }
    }

    /**
     * 获得 Method 的唯一签名
     * 格式 methodReturnTypeClassName#methodName:arg1,arg2
     * @param method
     * @return
     */
    private String getSignature(Method method){
        StringBuilder sb = new StringBuilder();
        Class<?> returnType = method.getReturnType();
        if(returnType != null){
            sb.append(returnType.getName()).append("#");
        }
        sb.append(method.getName());
        Class<?>[] parameterTypes = method.getParameterTypes();
        for (int i = 0; i < parameterTypes.length; i++) {
            if(i == 0){
                sb.append(":");
            } else {
                sb.append(",");
            }
            sb.append(parameterTypes[i].getName());
        }
        return sb.toString();
    }

    private static boolean canAccessPrivateMethods() {
        try {
            SecurityManager securityManager = System.getSecurityManager();
            if (null != securityManager) {
                securityManager.checkPermission(new ReflectPermission("suppressAccessChecks"));
            }
        } catch (SecurityException e) {
            return false;
        }
        return true;
    }

    public Class<?> getType() {
        return type;
    }

    public Constructor<?> getDefaultConstructor(){
        if(defaultConstructor != null){
            return defaultConstructor;
        } else {
            throw new RuntimeException("There is no default constructor for " + type);
        }
    }

    public boolean hasDefaultConstructor(){
        return defaultConstructor != null;
    }

    public Class<?> getSetterType(String propertyName){
        Class<?> clazz = setTypes.get(propertyName);
        if(clazz == null){
            throw new RuntimeException("There is no setter for property named '" + propertyName + "' in '" + type + "'");
        }
        return clazz;
    }

    public Invoker getGetInvoker(String propertyName){
        Invoker method = getMethods.get(propertyName);
        if (method == null) {
            throw new RuntimeException("There is no getter for property named '" + propertyName + "' in '" + type + "'");
        }
        return method;
    }

    public Invoker getSetInvoker(String propertyName){
        Invoker method = setMethods.get(propertyName);
        if (method == null) {
            throw new RuntimeException("There is no setter for property named '" + propertyName + "' in '" + type + "'");
        }
        return method;
    }

    public Class<?> getGetterType(String propertyName){
        Class<?> clazz = getTypes.get(propertyName);
        if (clazz == null) {
            throw new RuntimeException("There is no getter for property named '" + propertyName + "' in '" + type + "'");
        }
        return clazz;
    }

    public String[] getGetablePropertyNames(){
        return readablePropertyNames;
    }

    public String[] getSetablePropertyNames(){
        return writeablePropertyNames;
    }

    public boolean hasSetter(String propertyName){
        return setMethods.keySet().contains(propertyName);
    }

    public boolean hasGetter(String propertyName) {
        return getMethods.keySet().contains(propertyName);
    }

    public String findPropertyName(String name) {
        return caseInsensitivePropertyMap.get(name.toUpperCase(Locale.ENGLISH));
    }

    /**
     * 得到某个类的反射器，是静态方法，而且要缓存，又要多线程，所以 REFLECTOR_MAP 是一个 ConcurrentHashMap
     * @param clazz
     * @return
     */
    public static Reflector forClass(Class<?> clazz){
        if(classCacheEnabled){
            // synchronized (clazz) removed see issue #461
            // 因为这里已经用了并发安全的容器，所以这里便没有在加锁了
            // 对于每个类来说，我们假设它是不会变得，这样可以考虑将这个类的信息（构造函数、getter、setter、字段）加入缓存，以提高速度
            Reflector cached = REFLECTOR_MAP.get(clazz);
            if(cached == null){
                cached = new Reflector(clazz);
                REFLECTOR_MAP.put(clazz, cached);
            }
            return cached;
        }else{
            return new Reflector(clazz);
        }
    }

    public static void setClassCacheEnabled(boolean classCacheEnabled){
        Reflector.classCacheEnabled = classCacheEnabled;
    }

    public static boolean isClassCacheEnabled(){
        return classCacheEnabled;
    }
}
