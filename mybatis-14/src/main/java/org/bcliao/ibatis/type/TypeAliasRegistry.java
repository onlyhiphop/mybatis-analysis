package org.bcliao.ibatis.type;

import org.bcliao.ibatis.io.Resources;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * 类型别名注册机
 *
 * @author: bcliao
 * @date: 2022/8/10 11:21
 **/
public class TypeAliasRegistry {

    private final Map<String, Class<?>> TYPE_ALIASES = new HashMap<>();

    public TypeAliasRegistry(){
        //构造函数里注册系统内置的类型别名
        registerAlias("string", String.class);

        //基本包装类型
        registerAlias("byte", Byte.class);
        registerAlias("long", Long.class);
        registerAlias("short", Short.class);
        registerAlias("int", Integer.class);
        registerAlias("integer", Integer.class);
        registerAlias("double", Double.class);
        registerAlias("float", Float.class);
        registerAlias("boolean", Boolean.class);

        //加个下划线，就变成了基本类型
        registerAlias("_byte", byte.class);
        registerAlias("_long", long.class);
        registerAlias("_short", short.class);
        registerAlias("_int", int.class);
        registerAlias("_integer", int.class);
        registerAlias("_double", double.class);
        registerAlias("_float", float.class);
        registerAlias("_boolean", boolean.class);
    }

    public void registerAlias(String alias, Class<?> value){
        String key = alias.toLowerCase(Locale.ENGLISH);
        TYPE_ALIASES.put(key, value);
    }

    public <T> Class<T> resolveAlias(String string){
        try {
            if(string == null){
                return null;
            }
            String key = string.toLowerCase(Locale.ENGLISH);
            Class<T> value;
            if(TYPE_ALIASES.containsKey(key)){
                value = (Class<T>) TYPE_ALIASES.get(key);
            }else{
                value = (Class<T>) Resources.classForName(string);
            }
            return value;
        }catch (ClassNotFoundException e){
            throw new RuntimeException("Could not resolve type alias '" + string + "'.  Cause: " + e, e);
        }
    }

}
