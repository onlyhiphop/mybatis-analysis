package org.bcliao.ibatis.executor.keygen;

import org.bcliao.ibatis.executor.Executor;
import org.bcliao.ibatis.mapping.MappedStatement;

import java.sql.Statement;

/**
 * 
 * 键值生成器接口
 * 
 * @author bcliao
 * @date 2022/11/30 9:31
 */
public interface KeyGenerator {

    /**
     * 针对 Sequence 主键而言，在执行 insert sql 前必须指定一个主键值给要插入的记录
     * 如 Oracle、DB2、KeyGenerator 提供了processBefore() 方法
     */
    void processBefore(Executor executor, MappedStatement ms, Statement stmt, Object parameter);

    /**
     * 针对自增主键的表，在插入时不需要主键，而是在插入过程自动获取一个自增的主键
     * 比如 MySql、PostgreSQL、KeyGenerator 提供了processAfter() 方法
     */
    void processAfter(Executor executor, MappedStatement ms, Statement stmt, Object parameter);
}
