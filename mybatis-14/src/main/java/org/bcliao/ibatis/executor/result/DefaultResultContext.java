package org.bcliao.ibatis.executor.result;

import org.bcliao.ibatis.session.ResultContext;

/**
 * 
 * 默认结果上下文
 * 
 * @Author bcliao
 * @Date 2022/9/26 10:58
 */
public class DefaultResultContext implements ResultContext {

    private Object resultObject;
    private int resultCount;

    public DefaultResultContext() {
        this.resultObject = null;
        this.resultCount = 0;
    }

    @Override
    public Object getResultObject() {
        return resultObject;
    }

    @Override
    public int getResultCount() {
        return resultCount;
    }

    public void nextResultObject(Object resultObject) {
        resultCount++;
        this.resultObject = resultObject;
    }

}