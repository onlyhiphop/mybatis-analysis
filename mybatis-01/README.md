### 代理

当我们使用 mybatis 时，发现使用的 mapper 都是接口，这是因为 mybatis 使用了 jdk的动态代理。

代理有什么好处？

代理可以在原有实现类的基础上进行功能的扩展，而不改变原有的类。而动态代理是可以在运行时生成代理类，不需要手动创建，我们就不需要手动为每一个需要代理的类创建一个代理类，我们只需要关注需要扩展增强的功能，而这个扩展功能还可以给其他需要代理的类和接口复用。

![image-20220804145450742](img/image-20220804145450742.png)

jdk的动态代理需要三个参数：

- 加载 target 类的类加载器
- target类实现的接口
- InvocationHandle 调用处理器，我们需要扩展增强的地方



实际调用流程：当我们使用生成的代理类去调用方法时，在所有的方法中会调用 InvocationHandle 的 invoke 方法，所有该方法对代理类所有的方法做了增强。



最后生成的类：

```java
// 1、代理类首先继承了Proxy类（这也说明了为什么JDK代理需要实现接口，因为Java是单继承的），并且实现了目标接口Subject
public final class $Proxy0 extends Proxy implements Subject {
  	// 2、可以看到，在代理类中将我们的【目标接口Subject】的【所有方法（包括object父类的方法）】都以【静态属性的形式】保存了起来
    private static Method m1;
    private static Method m3;
    private static Method m4;
    private static Method m2;
    private static Method m0;
  
  	// 以静态代码块的形式为属性赋值
    static {
          try {
              m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
              m3 = Class.forName("com.stone.design.mode.proxy.jdk.Subject").getMethod("doSomething");
              m4 = Class.forName("com.stone.design.mode.proxy.jdk.Subject").getMethod("sayHello", Class.forName("java.lang.String"));
              m2 = Class.forName("java.lang.Object").getMethod("toString");
              m0 = Class.forName("java.lang.Object").getMethod("hashCode");
          } catch (NoSuchMethodException var2) {
              throw new NoSuchMethodError(var2.getMessage());
          } catch (ClassNotFoundException var3) {
              throw new NoClassDefFoundError(var3.getMessage());
          }
      }

    public $Proxy0(InvocationHandler var1) throws  {
        super(var1);
    }

  	// 3、object父类的equals方法
    public final boolean equals(Object var1) throws  {
        try {
          	// 这里的supper是指的Proxy类，调用【Proxy类】的【h属性】的invoke方法执行
          	// 重点：【注意这里的super.h】其实就是我们创建代理对象是传入的【InvocationHandler】，不信往后面看
          	// 这里也就体现了创建代理对象时为什么需要传入【InvocationHandler】，以及为什么调用代理对象的方法时都是执行的invoke方法
            return (Boolean)super.h.invoke(this, m1, new Object[]{var1});
        } catch (RuntimeException | Error var3) {
            throw var3;
        } catch (Throwable var4) {
            throw new UndeclaredThrowableException(var4);
        }
    }

  	// 4、目标接口的方法
    public final void doSomething() throws  {
        try {
          	// 调用了【Proxy.h属性】的invoke方法
          	// 注意这里的super.h】其实就是我们创建代理对象是传入的【InvocationHandler】，不信往后面看
            super.h.invoke(this, m3, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

  	// 5、目标接口的方法
    public final String sayHello(String var1) throws  {
        try {
          	// 调用了【Proxy.h属性】的invoke方法
          	// 注意这里的super.h】其实就是我们创建代理对象是传入的【InvocationHandler】，不信往后面看
            return (String)super.h.invoke(this, m4, new Object[]{var1});
        } catch (RuntimeException | Error var3) {
            throw var3;
        } catch (Throwable var4) {
            throw new UndeclaredThrowableException(var4);
        }
    }

    public final String toString() throws  {
        try {
            return (String)super.h.invoke(this, m2, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final int hashCode() throws  {
        try {
            return (Integer)super.h.invoke(this, m0, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

}

```

Proxy类：

```java
public class Proxy implements java.io.Serializable {
  
  	// h属性，保存我们传递进来的InvocationHandler
    protected InvocationHandler h;
  
    // 【有参构造器】注意这里的参数
    protected Proxy(InvocationHandler h) {
      Objects.requireNonNull(h);
      this.h = h;
    }

    // 生成代理对象的方法
    public static Object newProxyInstance(ClassLoader loader, Class<?>[] interfaces,InvocationHandler h)
        throws IllegalArgumentException{

      	// 1、InvocationHandler强制不允许为空
        Objects.requireNonNull(h);
        // 获取到目标接口
        final Class<?>[] intfs = interfaces.clone();

        /*
         * Look up or generate the designated proxy class.
         * 2、获取到代理类的Class对象（也就是Proxy）
         */
        Class<?> cl = getProxyClass0(loader, intfs);

        /*
         * Invoke its constructor with the designated invocation handler.
         * 通过反射执行 cl 的有参构造，也就是下面这个，可以看到通过反射执行Proxy有参构造，
         * 将InvocationHandler赋值到了h属性上
         */
        try {
            // 3、获取到有参构造器
            final Constructor<?> cons = cl.getConstructor(constructorParams);
            // 4、通过构造器来创建一个代理对象并返回，这里传入的参数h 就是我们的【InvocationHandler】
            return cons.newInstance(new Object[]{h});
        } catch (IllegalAccessException|InstantiationException e) {
            // 省略....
        }
    }
}
```

