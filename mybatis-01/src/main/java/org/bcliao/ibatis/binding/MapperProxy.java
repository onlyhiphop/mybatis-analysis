package org.bcliao.ibatis.binding;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author: bcliao
 * @date: 2022/8/4 11:07
 *
 * 映射器代理类
 **/
public class MapperProxy<T> implements InvocationHandler, Serializable {

    private Map<String, Object> sqlSession;
    private Class<T> mapperInterface;

    public MapperProxy(Map<String, Object> sqlSession, Class<T> mapperInterface) {
        this.sqlSession = sqlSession;
        this.mapperInterface = mapperInterface;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //Object提供的例如toString或者hashCode等方法是不需要被代理的，所以这里加个判断
        if(Object.class.equals(method.getDeclaringClass())){
            //直接调用，不做处理
            System.out.println("直接调用不处理----");
            return method.invoke(this, args);
        }
        return sqlSession.get(mapperInterface.getName() + "." + method.getName());
    }

}
