package org.bcliao.mybatis.test;

import org.bcliao.ibatis.binding.MapperProxyFactory;
import org.bcliao.mybatis.test.dao.IUserDao;
import org.junit.Test;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: bcliao
 * @date: 2022/8/4 13:48
 **/
public class MainTest {

    @Test
    public void test_MapperProxyFactory(){
        MapperProxyFactory<IUserDao> factory = new MapperProxyFactory<>(IUserDao.class);
        Map<String, Object> sqlSession = new HashMap<>(){{
            put("org.bcliao.mybatis.test.dao.IUserDao.queryUserName", "张三");
            put("org.bcliao.mybatis.test.dao.IUserDao.queryUserAge", 18);
        }};
        IUserDao userDao = factory.newInstance(sqlSession);
        String userName = userDao.queryUserName("001");
        Integer age = userDao.queryUserAge("001");
        System.out.println(userName);
        System.out.println(age);

        System.out.println(userDao.toString());
    }


    @Test
    public void test_proxy(){
        IUserDao userDao = (IUserDao) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{IUserDao.class},
                (proxy, method, args) -> "查询UID："+ args[0] + " 方法被代理了");
        String s = userDao.queryUserName("001");
        System.out.println(s);
    }
}
