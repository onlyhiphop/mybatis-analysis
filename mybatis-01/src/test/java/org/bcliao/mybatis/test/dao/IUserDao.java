package org.bcliao.mybatis.test.dao;

/**
 * @author: bcliao
 * @date: 2022/8/4 13:49
 **/
public interface IUserDao {

    String queryUserName(String uid);

    Integer queryUserAge(String uid);
}
